import './AboutUs.css';
import React from 'react';
import Deyan from '../../assets/aboutus/deyan.jpg';
import Daniel from '../../assets/aboutus/daniel.jpg';
import Boyko from '../../assets/aboutus/boyko.jpg';

const AboutUs = () => {
  const GitLabDeyan = 'https://gitlab.com/Simeodey';
  const LinkedInDeyan = 'https://www.linkedin.com/in/deyan-s-21a6a7151';
  const GitLabDaniel =
    'https://gitlab.com/Simeodey/final_project-fitness_tracker';
  const GitLabBoyko =
    'https://gitlab.com/Simeodey/final_project-fitness_tracker';

  return (
    <div className='about-us-container'>
      <div className='about-us-box'>
        <h2>About Us</h2>
        <div className='info'>
          <div className='block'>
            <img src={Deyan} alt='Deyan Simeonov' />
            <div className='text'>
              <div>Email: deyan.g.simeonov@gmail.com</div>
              <a href={LinkedInDeyan}>LinkedIn</a>
              <a href={GitLabDeyan}>GitLab</a>
            </div>
          </div>
          <div className='block'>
            <img src={Daniel} alt='Daniel Dimitrov' />
            <div className='text'>
              <div>Email: dani@gmail.com</div>
              <div>LinkedIn: qwefwfgawrgawwe</div>
              <a href={GitLabDaniel}>GitLab</a>
            </div>
          </div>
          <div className='block'>
            <img src={Boyko} alt='Boyko Denev' />
            <div className='text'>
              <div>Email: boyko74@gmail.com</div>
              <div>LinkedIn: qwefwfgawrgawwe</div>
              <a href={GitLabBoyko}>GitLab</a>
            </div>
          </div>
        </div>
        <div className='Telerik'>
          <div>Telerik Academy</div>
          <div>Alpha JavaScript Track</div>
          <div>2022</div>
        </div>
      </div>
    </div>
  );
};

export default AboutUs;
