import './Home.css';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import HomeImage from '../../assets/homepage/homepage.svg';

const Home = () => {
  const navigate = useNavigate();

  return (
    <div className='home-page-container'>
      <img className='home-page-image' src={HomeImage} alt='homeimage' />
      <div className='home-page-text'>
        <div>Welcome to our world!</div>
        <div>Play sport, have fun and become a hero.</div>
        <button onClick={() => navigate('/login', { replace: true })}>
          Join us
        </button>
      </div>
      <div
        className='home-page-footer'
        onClick={() => navigate('/about-us', { replace: true })}>
        About Us
      </div>
    </div>
  );
};

export default Home;
