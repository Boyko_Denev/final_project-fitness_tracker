import './ProfileCompleted.css';
import React, { useState } from 'react';
import { updateUserDocumentFields } from '../../services/users.service';
import { useLocation, useNavigate } from 'react-router-dom';
import { toastOptions } from '../../common/constants';
import { toast } from 'react-toastify';
import {
  AGE_MIN,
  AGE_MAX,
  HEIGHT_MIN,
  HEIGHT_MAX,
  WEIGHT_MIN,
  WEIGHT_MAX,
} from '../../common/constants';

export const ProfileCompleted = () => {
  const [ageError, setAgeError] = useState('');
  const [heightError, setHeightError] = useState('');
  const [weightError, setWeightError] = useState('');
  const location = useLocation();
  const navigate = useNavigate();

  const [form, setForm] = useState({
    age: '',
    height: '',
    weight: '',
  });

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: Number(e.target.value),
    });
  };

  const validateAge = () => {
    setAgeError('');

    if (form.age < AGE_MIN || form.age > AGE_MAX) {
      setAgeError(`Set valid Age (${AGE_MIN} - ${AGE_MAX})`);
    }
  };

  const validateHeight = () => {
    setHeightError('');

    if (form.height < HEIGHT_MIN || form.height > HEIGHT_MAX) {
      setHeightError(`Set valid Height (${HEIGHT_MIN}-${HEIGHT_MAX})`);
    }
  };

  const validateWeight = () => {
    setWeightError('');

    if (form.weight < WEIGHT_MIN || form.weight > WEIGHT_MAX) {
      setWeightError(`Set valid Weight (${WEIGHT_MIN}-${WEIGHT_MAX})`);
    }
  };

  const submit = () => {
    updateUserDocumentFields('/Users', location.state, form)
      .then(() => {
        updateUserDocumentFields('/Users', location.state, {
          isProfileCompleted: true,
        })
          .then(() => {
            navigate(location?.state?.from ?? '/login', {
              replace: true,
            });
          })
          .catch((err) => {
            toast.error(err.message, toastOptions);
          });
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  };

  return (
    <div className='profile-completed'>
      <div className='profile-completed-box'>
        <h2>Complete Profile</h2>
        <div className='Form'>
          <div className='stack'>
            <label htmlFor='age'>Age: </label>
            <input
              type='number'
              id='age'
              value={form.age}
              onChange={updateForm('age')}
              onBlur={validateAge}></input>
          </div>
          <div className='notes'>
            <p>age</p>
            {ageError && <div className='error'>{ageError}</div>}
          </div>

          <div className='stack'>
            <label htmlFor='height'>Height: </label>
            <input
              type='number'
              id='height'
              value={form.height}
              onChange={updateForm('height')}
              onBlur={validateHeight}></input>
          </div>
          <div className='notes'>
            <p>cm</p>
            {heightError && <div className='error'>{heightError}</div>}
          </div>

          <div className='stack'>
            <label htmlFor='weight'>Weight: </label>
            <input
              type='number'
              id='weight'
              value={form.weight}
              onChange={updateForm('weight')}
              onBlur={validateWeight}></input>
          </div>
          <div className='notes'>
            <p>kg</p>
            {weightError && <div className='error'>{weightError}</div>}
          </div>

          <button
            onClick={submit}
            disabled={ageError || heightError || weightError}>
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};
