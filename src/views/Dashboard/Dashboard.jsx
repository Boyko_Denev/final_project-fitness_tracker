import 'antd/dist/antd.min.css';
import { extractDate } from '../../utils/dateFomarting.js';
import moment from 'moment';
import { Filters } from '../../components/Dashboard/Filters.jsx';
import { ActivitySelection } from '../../components/Dashboard/ActivitySelection.jsx';
import { useEffect, useState } from 'react';
import { ActivityCharts } from '../../components/Dashboard/ActivityCharts.jsx';
import './Dashboard.css';
import { activitiesData } from '../../data/mockActivities.js';
import { Button } from 'antd';
import { getUserGoals } from '../../services/goals.service.js';
import { Stat } from '../../components/Dashboard/Stat.jsx';
import { toast, ToastContainer } from 'react-toastify';
import { toastOptions } from '../../common/constants.js';
import { useLocation } from 'react-router-dom';
import ClipLoader from 'react-spinners/ClipLoader';
import {
  getAllActivities,
  getUserActivities,
} from '../../services/activities.service.js';
import { RadialChart } from '../../components/Dashboard/RadialChart.jsx';
import { WaterDaily } from '../../components/waterDaily/WaterDaily';
import RecentActivities from '../../components/RecentActivities/RecentActivities.jsx';
import WaterDailyChart from '../../components/WaterDailyChart/WaterDailyChart.jsx';
const fetchActivities = () => {
  return getAllActivities();
};

const fetchUserActivities = () => {
  return getUserActivities(sessionStorage.uid);
};

export const Dashboard = () => {
  const [activityData, setActivityData] = useState(null);
  const [userActivities, setUserActivities] = useState(null);
  const [selectedActivities, setSelectedActivities] = useState([]);
  const [dateSelection, setDateSelection] = useState('day');
  const [data, setData] = useState([]);
  const [agregateData, setAgregateData] = useState([]);
  const [pointsGoalsData, setPointsGoalsData] = useState({});
  const [stepsGoalsData, setStepsGoalsData] = useState({});
  const [goalsTypeSelection, setGoalsTypeSelection] = useState('points');
  const [isLoading, setIsLoading] = useState(true);
  const location = useLocation();

  useEffect(() => {
    data &&
      setAgregateData(
        data.reduce(
          (acc, cur) => {
            acc.totalPoints = acc.totalPoints + cur.totalPoints;
            acc.totalTime = acc.totalTime + cur.totalTime;
            return acc;
          },
          {
            totalPoints: 0,
            totalTime: 0,
          },
        ),
      );
  }, [data]);

  useEffect(() => {
    if (location.state) {
      toast.success(`Welcome, ${location.state}!`, toastOptions);
    }
    fetchActivities().then((data) => {
      setActivityData(data);
    });
  }, []);

  useEffect(() => {
    fetchUserActivities().then((data) => {
      setUserActivities(data);
    });
  }, []);

  useEffect(() => {
    getUserGoals(sessionStorage.uid)
      .then((resp) => {
        resp.forEach((el) => {
          if (el.type === 'points') {
            setPointsGoalsData({
              name: el.name,
              start: extractDate(el.start.seconds),
              end: extractDate(el.end.seconds),
              value: el.totalPoints,
              target: el.target,
            });
          }
          if (el.type === 'steps') {
            setStepsGoalsData({
              name: el.name,
              start: extractDate(el.start.seconds),
              end: extractDate(el.end.seconds),
              value: el.totalSteps,
              target: el.target,
            });
          }
        });
        setIsLoading(false);
      })
      .catch((e) => {
        toast.error('Something went wrong!', toastOptions);
        console.log(e);
      });
  }, []);

  return (
    <div className='main-board-container'>
      <div className='goals-stats-list'>
        <div className={'dashboard-goals'}>
          <h2 style={{ textAlign: 'center' }}>Current Active Goals</h2>
          <h6 style={{ textAlign: 'center' }}>
            {
              (goalsTypeSelection === 'points'
                ? pointsGoalsData
                : stepsGoalsData)['name']
            }
          </h6>
          <h6 style={{ textAlign: 'center' }}>{`${
            (goalsTypeSelection === 'points'
              ? pointsGoalsData
              : stepsGoalsData)['start']
          } - ${
            (goalsTypeSelection === 'points'
              ? pointsGoalsData
              : stepsGoalsData)['end']
          }`}</h6>
          <div className='buttons-goals'>
            <Button
              type={goalsTypeSelection === 'points' ? 'danger' : 'primary'}
              onClick={() => {
                setGoalsTypeSelection('points');
              }}>
              Points
            </Button>
            <Button
              type={goalsTypeSelection === 'steps' ? 'danger' : 'primary'}
              onClick={() => {
                setGoalsTypeSelection('steps');
              }}>
              Steps
            </Button>
          </div>
          {isLoading ? (
            <div className='Spinner'>
              <ClipLoader
                color={'rgb(0, 45, 41)'}
                loading={isLoading}
                size={100}
                speedMultiplier='0.7'
              />
            </div>
          ) : (
            <div className='radial-chart'>
              <RadialChart
                data={
                  goalsTypeSelection === 'points'
                    ? pointsGoalsData
                    : stepsGoalsData
                }
              />
            </div>
          )}
        </div>
        <div className='recent-activities'>
          <RecentActivities />
        </div>
      </div>
      <div className='chart-stats'>
        <div className='container-dashboard'>
          <Filters prop={{ dateSelection, setDateSelection }} />
          <ActivitySelection
            prop={{ activityData, selectedActivities, setSelectedActivities }}
          />
          <ActivityCharts
            prop={{
              userActivities,
              dateSelection,
              selectedActivities,
              setData,
            }}
          />
        </div>
        <div className={'dashboard-stats'}>
          <Stat
            data={agregateData.totalTime}
            suffix={'min'}
            title={'Total activities time'}
          />
          <Stat
            data={agregateData.totalPoints}
            suffix={'pts'}
            title={'Total points'}
          />
        </div>
      </div>
      <div className='water-daily-chart'>
        <div>
          <WaterDaily />
        </div>
        <div>
          <WaterDailyChart />
        </div>
      </div>
    </div>
  );
};
