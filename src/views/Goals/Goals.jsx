import {Tooltip} from 'antd';
import weight from '../../assets/images/losing-weight.svg';
import boxing from '../../assets/images/boxing.svg';
import steps from '../../assets/images/steps.svg';
import water from '../../assets/images/water.svg';
import {Avatar} from '../../components/Goals/Avatar/Avatar.jsx'
import './Goals.css';
import {useEffect, useState} from 'react';
import {AddGoal} from '../../components/Goals/AddGoal/AddGoal.jsx';
import {getAllGoalsFromDb} from '../../services/goals.service.js';


export const Goals = () => {
  const [openGoal,setOpenGoal] = useState(false)
  const [typeOfGoal,setTypeOfGoal] = useState('')
  const [goals,setGoals] = useState([])
  const [loading,setLoading] = useState()
  useEffect(()=>{
   if(typeOfGoal) setOpenGoal(true)

  },[typeOfGoal])

  useEffect(()=>{
    getAllGoalsFromDb().then(g=>{
      setGoals(g)
    })
  },[])
  return (
    <div className="wrapperGoals">

      <div className={'wrapperGoals-title'}><h1>Choose your goal:</h1></div>
      <div className="goalsGridContainer">

        {goals.map((goal,i)=>{


        return (<Tooltip title={goal.tooltip} key={i}>
          <div className='goalSelection'>
            {/*<Link to='addGoal' smooth={true} duration={100} delay={150}>*/}
            <Avatar
              style={goal.style}
              size={goal.size}
              icon={goal.icon}
              onClick={()=>{
                setTypeOfGoal(goal.goalName)
                const openGoal = new Promise((resolve)=>{
                  return resolve(setOpenGoal(true))
                })
                openGoal.then(()=> document.getElementById('addGoal').scrollIntoView({behavior:'smooth'}))
              }}
            />
            {/*</Link>*/}
            <p>{goal.description}</p>
          </div>
        </Tooltip>
        )})
        }

      </div>


    {openGoal &&
        <AddGoal props={{setOpenGoal,typeOfGoal}} />
    }
    </div>
  );
};