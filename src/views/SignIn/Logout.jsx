import './Logout.css';
import React, { useContext, useState } from 'react';
import AppState from '../../providers/app-state';
import { logoutUser } from '../../services/auth.service';
import { useNavigate, useLocation } from 'react-router-dom';
import { toast } from 'react-toastify';
import { toastOptions } from '../../common/constants';

const Logout = () => {
  const { appState, setState } = useContext(AppState);
  const navigate = useNavigate();
  const location = useLocation();

  const logout = () => {
    setState({
      user: null,
      userData: null,
    });

    sessionStorage.clear();

    logoutUser()
      .then(() => {
        navigate('/');
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  };

  const cancel = () => {
    navigate(location?.state?.from ?? '/board', {
      replace: true,
    });
  };

  return (
    <div className='logout-container'>
      <div className='logout-box'>
        <h2>
          <span>Are you sure</span> <span>you want to log out?</span>
        </h2>
        <button onClick={logout}>Log Out</button>
        <button onClick={cancel}>Cancel</button>
      </div>
    </div>
  );
};

export default Logout;
