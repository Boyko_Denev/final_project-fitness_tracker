import './Login.css';
import React, { useState, useContext } from 'react';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import AppState from '../../providers/app-state';
// import { loginUser } from '../../services/auth.service';
import { getUserData, updateDocumentField } from '../../services/users.service';
import { toast, ToastContainer } from 'react-toastify';
import { REGEX_EMAIL, toastOptions } from '../../common/constants';
import {
  forgottenPassword,
  loginGooglePopUp,
  loginUser,
} from '../../services/auth.service';
import { getAllFriends } from '../../services/friends.service';
import { signup } from './Signup.jsx';

const Login = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [emailError, setEmailError] = useState('');
  const { appState, setState } = useContext(AppState);
  const [passwordShown, setPasswordShown] = useState(false);

  const togglePassword = () => {
    setPasswordShown(!passwordShown);
  };

  const [form, setForm] = useState({
    email: '',
    password: '',
  });

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const clearEmailError = () => {
    setEmailError('');
  };

  const validateEmail = () => {
    const filterEmail = REGEX_EMAIL;

    if (!filterEmail.test(form.email) || form.email === '') {
      setEmailError('Please provide a valid Email Address');

      return false;
    }
  };

  const handleLogin = (authProvider) => {
    if (authProvider === 'password') {
      return loginUser(form.email, form.password);
    } else if (authProvider === 'google') {
      return loginGooglePopUp();
    }
  };
  const login = (authProvider) => {
    // e.preventDefault();

    handleLogin(authProvider)
      .then((userData) => {
        const { isAnonymous, uid, email, displayName, photoURL, providerData } =
          userData;
        if (isAnonymous === false) {
          getUserData('/Users', uid)
            .then((resp) => {
              if (!resp.data()) {
                signup(email, email, '', '', uid, providerData);
              }
              if (resp.data()?.isBlocked) {
                toast.error(
                  `You are blocked! Please contact an Admin`,
                  toastOptions,
                );
              } else if (!resp.data()?.isProfileCompleted) {
                navigate(
                  location?.state?.from ?? '/profile-completed',
                  { state: uid },
                  {
                    replace: true,
                  },
                );
              } else {
                const currentUserData = resp.data();
                getAllFriends(uid)
                  .then((data) => {
                    setState({
                      user: uid,
                      userData: { ...currentUserData },
                      friends: data,
                    });
                  })
                  .catch((err) => {
                    toast.error('Something went wrong!', toastOptions);
                  });
                for (let [key, value] of Object.entries({
                  uid: currentUserData?.uid,
                  email: currentUserData?.email,
                  isAnonymous: isAnonymous,
                  avatarUrl: currentUserData?.avatarUrl,
                  lastLoginAt: currentUserData?.lastLoginAt,
                  userName: currentUserData?.userName,
                  firstName: currentUserData?.firstName,
                  lastName: currentUserData?.lastName,
                  role: currentUserData?.role,
                  info: currentUserData?.info,
                  weight: currentUserData?.weight,
                  height: currentUserData?.height,
                  age: currentUserData?.age,
                })) {
                  sessionStorage.setItem(key, value);
                }
                navigate(location?.state?.from ?? '/board', {
                  state: resp.data().userName,
                  replace: true,
                });
              }
            })
            .catch((err) => {
              toast.error(err.message, toastOptions);
            });
        }
      })
      .catch((err) => {
        if (err.message.includes(`auth/user-not-found`)) {
          toast.error(`User does not exist`, toastOptions);
        } else if (err.message.includes(`auth/invalid-email`)) {
          toast.error(`Email ${form.email} does not exist`, toastOptions);
        } else if (err.message.includes('auth/wrong-password')) {
          toast.error(`Wrong password`, toastOptions);
        } else if (err.message.includes('auth/too-many-requests')) {
          toast.error(
            `Access to this account has been temporarily disabled due to many failed login attempts. You can immediately restore it by resetting your password or you can try again later.`,
            toastOptions,
          );
        } else {
          toast.error(err.message, toastOptions);
        }
      });
  };

  const handleClickForgottenPassword = () => {
    forgottenPassword(form.email)
      .then(() => {
        toast.success(
          'Password reset email sent. Check your email or spam for instructions.',
          toastOptions,
        );
      })
      .catch((err) => {
        if (err.message.includes('auth/missing-email')) {
          toast.error('Fill in your email', toastOptions);
        } else {
          toast.error(err.message, toastOptions);
        }
      });
  };

  return (
    <>
      <ToastContainer />
      <div className='login-container'>
        <div className='login-image-box'></div>
        <div className='login-box'>
          <h2>Log In</h2>
          <div className='Form'>
            <label htmlFor='email'>Email: </label>
            <input
              type='email'
              id='email'
              value={form.email}
              onChange={updateForm('email')}
              onBlur={validateEmail}
              onFocus={clearEmailError}></input>
            <div className='error'>{emailError}</div>

            <label htmlFor='password'>Password: </label>
            <div className='pass-and-eye'>
              <input
                type={passwordShown ? 'text' : 'password'}
                id='password'
                value={form.password}
                onChange={updateForm('password')}
                onKeyDown={(e) => {
                  e.key === 'Enter' && login('password');
                }}></input>
              <i
                className={passwordShown ? 'fas fa-eye' : 'fas fa-eye-slash'}
                id='togglePassword'
                style={{
                  marginLeft: '-30px',
                  cursor: 'pointer',
                  visibility: form.password ? 'visible' : 'hidden',
                }}
                onClick={togglePassword}></i>
            </div>
            <div
              onClick={handleClickForgottenPassword}
              className='forgotten-password'>
              Forgotten Password
            </div>
            <button
              onClick={() => login('password')}
              disabled={!form.email || !form.password}>
              Log In
            </button>
            <button onClick={() => login('google')}>Login with Google </button>
            <p>
              Not a member? <Link to='/signup'>Sign Up</Link>
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
