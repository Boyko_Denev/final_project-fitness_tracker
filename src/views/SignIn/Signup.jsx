import './Signup.css';
import React, { useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { signupUser } from '../../services/auth.service.js';
import {
  createUserUsername,
  getUserById,
  getUserByUsername,
} from '../../services/users.service';
import { toast, ToastContainer } from 'react-toastify';
import {
  FILTER_DIGITS,
  FILTER_LOWERCASE,
  FILTER_SPECIAL_CHAR,
  FILTER_UPPERCASE,
  REGEX_EMAIL,
  REGEX_PHONE,
  toastOptions,
  VALIDATION_PASSWORD_LENGTH,
  VALIDATION_USERNAME_LENGTH,
} from '../../common/constants';

export const signup = (
  userName,
  email,
  password,
  phoneNumber,
  uid = null,
  providerData = 'password',
) => {
  return getUserByUsername(userName).then((res) => {
    if (res.size === 0) {
      if (providerData[0]?.providerId === 'google.com') {
        getUserById(uid).then((isAlive) => {
          if (!isAlive.data()) {
            createUserUsername(
              userName,
              phoneNumber,
              uid,
              email,
              'Google Authentication',
            ).then(() => null);
          }
        });
      } else if (providerData === 'password') {
        return signupUser(email, password).then((authResp) => {
          return createUserUsername(
            userName,
            phoneNumber,
            authResp.user.uid,
            email,
            'Password Authentication',
          )
            .then(() => {
              return authResp;
            })
            .catch((err) => {
              if (err.message.includes(`auth/email-already-in-use`)) {
                toast.error(
                  `Email ${email} has already been registered!`,
                  toastOptions,
                );
              } else {
                toast.error(err.message, toastOptions);
              }
            });
        });
      }
    }
  });
};

const Signup = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [userNameError, setUserNameError] = useState('');
  const [phoneNumberError, setPhoneNumberError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');
  const [confirmPass, setConfirmPass] = useState('');

  const [form, setForm] = useState({
    userName: '',
    phoneNumber: '',
    email: '',
    password: '',
  });

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const clearUserNameError = () => {
    setUserNameError('');
  };

  const validateUserName = () => {
    if (form.userName.length < VALIDATION_USERNAME_LENGTH) {
      setUserNameError(
        `User Name too short (${VALIDATION_USERNAME_LENGTH} symbols min)`,
      );
    }
  };

  const clearPhoneNumberError = () => {
    setPhoneNumberError('');
  };

  const validatePhoneNumber = () => {
    const filterPhone = REGEX_PHONE;

    if (!filterPhone.test(form.phoneNumber) || form.phoneNumber === '') {
      setPhoneNumberError('Please provide a valid Phone Number');

      return false;
    }
  };

  const clearEmailError = () => {
    setEmailError('');
  };

  const validateEmail = () => {
    const filterEmail = REGEX_EMAIL;

    if (!filterEmail.test(form.email) || form.email === '') {
      setEmailError('Please provide a valid Email Address');

      return false;
    }
  };

  const clearPasswordError = () => {
    setPasswordError('');
  };

  const validatePassword = () => {
    if (form.password.length === 0) {
      setPasswordError('Password is empty');
    } else if (!FILTER_UPPERCASE.test(form.password)) {
      setPasswordError('At least one Uppercase');
    } else if (!FILTER_LOWERCASE.test(form.password)) {
      setPasswordError('At least one Lowercase');
    } else if (!FILTER_DIGITS.test(form.password)) {
      setPasswordError('At least one digit');
    } else if (!FILTER_SPECIAL_CHAR.test(form.password)) {
      setPasswordError('At least one Special Characters');
    } else if (form.password.length < VALIDATION_PASSWORD_LENGTH) {
      setPasswordError(
        `Password too short (${VALIDATION_PASSWORD_LENGTH} symbols min)`,
      );
    } else {
      setPasswordError('');
    }
  };

  const clearConfirmPasswordError = () => {
    setConfirmPasswordError('');
  };

  const validateConfirmPassword = () => {
    if (confirmPass !== form.password) {
      setConfirmPasswordError('Wrong Password');
    }
  };

  const handleSignup = (userName, email, password, phoneNumber) => {
    signup(userName, email, password, phoneNumber)
      .then((authResp) => {
        authResp &&
          navigate(
            location?.state?.from ?? '/profile-completed',
            { state: authResp.user?.uid },
            {
              replace: true,
            },
          );
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  };
  const handleKeyPressSignUp = (event) => {
    if (event.key === 'Enter') {
      signup(event);
    }
  };
  return (
    <div className='signup-container'>
      <ToastContainer />
      <div className='signup-image-box'></div>
      <div className='signup-box'>
        <h2>Sign Up</h2>
        <div className='Form'>
          <label htmlFor='user-name'>User Name: </label>
          <input
            type='text'
            id='userName'
            value={form.userName}
            onChange={updateForm('userName')}
            onBlur={validateUserName}
            onFocus={clearUserNameError}></input>
          <div className='error'>{userNameError}</div>

          <label htmlFor='phoneNumber'>Phone Number: </label>
          <input
            type='text'
            id='phoneNumber'
            value={form.phoneNumber}
            onChange={updateForm('phoneNumber')}
            onBlur={validatePhoneNumber}
            onFocus={clearPhoneNumberError}></input>
          <div className='error'>{phoneNumberError}</div>

          <label htmlFor='email'>Email: </label>
          <input
            type='email'
            id='email'
            value={form.email}
            onChange={updateForm('email')}
            onBlur={validateEmail}
            onFocus={clearEmailError}></input>
          <div className='error'>{emailError}</div>

          <label htmlFor='password'>Password: </label>
          <input
            type='password'
            id='password'
            value={form.password}
            onChange={updateForm('password')}
            onBlur={validatePassword}
            onFocus={clearPasswordError}></input>
          <div className='error'>{passwordError}</div>

          <label htmlFor='confirmPassword'>Confirm Password: </label>
          <input
            type='password'
            id='confirmPassword'
            value={confirmPass}
            onChange={(e) => setConfirmPass(e.target.value)}
            onBlur={validateConfirmPassword}
            onFocus={clearConfirmPasswordError}
            onKeyDown={handleKeyPressSignUp}></input>
          <div className='error'>{confirmPasswordError}</div>
          <button
            onClick={() =>
              handleSignup(
                form.userName,
                form.email,
                form.password,
                form.phoneNumber,
              )
            }
            disabled={
              !(form.userName && userNameError === '') ||
              !(form.email && emailError === '') ||
              !(form.password && passwordError === '') ||
              !(form.phoneNumber && phoneNumberError === '') ||
              !(
                confirmPass.length === form.password.length &&
                confirmPasswordError === ''
              )
            }>
            Sign Up
          </button>
          <p>
            You have an account? <Link to='/login'>Log In</Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Signup;
