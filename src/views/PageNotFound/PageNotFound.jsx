import homer from '../../assets/images/homer.png';
import './PageNotFound.css';

export const Error404 = () => (
  <div className='error-container'>
    <h1>D'OH 404</h1>
    <img src={homer} alt='homer'></img>
    <h2>Page not found</h2>
    <p>The page you are looking for doesn't exist.</p>
  </div>
);
