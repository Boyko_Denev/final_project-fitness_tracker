import ProgressBar from '../../components/progressbar/ProgressBar.jsx';
import { extractDate } from '../../utils/dateFomarting.js';
import { Badge } from 'antd';
import { useState, useRef, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { UserPicture } from '../../components/UserPicture/UserPicture.jsx';
import { upload } from '../../services/storage.service';
import '../Profile/ProfilePage.css';
import nextLevel from '../../utils/expCalculator';
import {
  updateUserDocumentField,
  getUserById,
} from '../../services/users.service';
import { UsersCalendar } from '../../components/UsersCalendar/UsersCalendar.jsx';
import { getUserActivities } from '../../services/activities.service.js';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencil } from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';
import {
  toastOptions,
  gender,
  ALLOWED_IMAGE_EXTENSIONS,
  AGE_MAX,
  AGE_MIN,
  HEIGHT_MAX,
  HEIGHT_MIN,
  WEIGHT_MAX,
  WEIGHT_MIN,
  activityColorCoding,
} from '../../common/constants';
import Tabs from '../../components/tabs/Tabs';

const Profile = () => {
  const [inputs, setInputs] = useState({});
  const [userInfo, setUserInfo] = useState({});
  const [loading, setLoading] = useState(true);
  const [editable, setEditable] = useState(false);
  const [userActivities, setUserActivities] = useState([]);
  const [tab, setTab] = useState('1');
  const [ageError, setAgeError] = useState('');
  const [heightError, setHeightError] = useState('');
  const [weightError, setWeightError] = useState('');
  const hiddenFileInput = useRef(null);
  const { uid: userId } = useParams();

  useEffect(() => {
    getUserById(userId)
      .then((resp) => {
        setUserInfo(resp.data());
        getUserActivities(userId).then((data) => {
          setUserActivities(data);
          setLoading(false);
        });
      })
      .catch((e) => console.log(e.message));
  }, []);

  const changeImage = (event) => {
    if (hiddenFileInput.current) {
      hiddenFileInput.current.click();
    }
  };
  const validateAge = (event) => {
    setAgeError('');

    if (event.target.value < AGE_MIN || event.target.value > AGE_MAX) {
      setAgeError(`Set valid Age (${AGE_MIN} - ${AGE_MAX})`);
    }
  };

  const validateHeight = (event) => {
    setHeightError('');

    if (event.target.value < HEIGHT_MIN || event.target.value > HEIGHT_MAX) {
      setHeightError(`Set valid Height (${HEIGHT_MIN}-${HEIGHT_MAX})`);
    }
  };

  const validateWeight = (event) => {
    setWeightError('');

    if (event.target.value < WEIGHT_MIN || event.target.value > WEIGHT_MAX) {
      setWeightError(`Set valid Weight (${WEIGHT_MIN}-${WEIGHT_MAX})`);
    }
  };
  const handleImageUpload = (e) => {
    const file = e.target.files[0];
    console.log(file);
    if (!file) return toast.error('Please select a file!', toastOptions);
    if (!ALLOWED_IMAGE_EXTENSIONS.exec(file.name)) {
      return toast.error(
        'You can upload file having extensions .jpeg/.jpg/.png only.',
        toastOptions,
      );
    }
    toast
      .promise(
        upload(file, `${userId}.png`, 'UserAvatar'),
        {
          pending: 'File is uploading',
          success: 'File Uploaded!',
          error: 'Something went wrong!',
        },
        toastOptions,
      )
      .then((url) => {
        toast
          .promise(
            updateUserDocumentField('/Users', userId, { avatarUrl: url }),
            {
              pending: 'Updating profile photo',
              success: 'Profile photo Uploaded!',
              error: 'Something went wrong!',
            },
            toastOptions,
          )
          .then(() => {
            setUserInfo({ ...userInfo, avatarUrl: url });
            sessionStorage.setItem('avatarUrl', url);
          })
          .catch((e) => console.log(e.message));
      })
      .catch((e) => console.log(e.message));
  };
  const handleUpdateProfile = () => {
    if (Object.keys(inputs).length === 0) {
      setEditable(!editable);
      return toast.info('No changes have been made', toastOptions);
    }
    toast
      .promise(
        updateUserDocumentField('/Users', userId, inputs),
        {
          pending: 'Updating profile info',
          success: 'Profile updated successfully!',
          error: 'Something went wrong!',
        },
        toastOptions,
      )
      .then(() => {
        setUserInfo({ ...userInfo, ...inputs });
        for (const element in inputs) {
          sessionStorage.setItem(element, inputs[element]);
        }
        setEditable(!editable);
      })
      .catch((e) => console.log(e.message));
  };

  const handleInput = (event) => {
    const name = event.target.name;
    let value = event.target.value;
    if (['age', 'height', 'weight', 'gender'].includes(name)) {
      value = Number(value);
    }
    setInputs((values) => ({ ...values, [name]: value }));
  };

  const handleTabs = (selectedTab) => {
    setTab(selectedTab);
  };
  return loading ? (
    <div className='App'>Loading...</div>
  ) : (
    <div className='profile-container'>
      <div className='wrapperProfile'>
        <div className='left'>
          <div className='userData'>
            <div className='left-container'>
              <UserPicture
                avatarUrl={userInfo?.avatarUrl ? userInfo.avatarUrl : null}
                userName={userInfo.userName}
                className='user-picture'
                size={100}
              />
              <div className='overlay'>
                <button className='icon' onClick={changeImage}>
                  <FontAwesomeIcon icon={faPencil} />
                </button>
              </div>
              <input
                style={{ display: 'none' }}
                onClick={changeImage}
                ref={hiddenFileInput}
                type='file'
                id='fileInput'
                name='attachement'
                disabled={loading}
              />
              <input
                style={{ display: 'none' }}
                onChange={handleImageUpload}
                ref={hiddenFileInput}
                type='file'
                id='fileInput'
                name='attachement'
              />
            </div>
            <div className='name-stats'>
              <h4 style={{ color: '#fff' }}>
                {userInfo?.userName ? userInfo.userName : 'Johnny'}
              </h4>
              <ProgressBar
                className='progressBar'
                bgcolor='#6a1b9a'
                completed={`${Math.round(
                  (100 * userInfo.currentExperience) /
                    nextLevel(userInfo.level),
                )}`}
                level={userInfo.level}
                value={`${userInfo.currentExperience}/${nextLevel(
                  userInfo.level,
                )}`}></ProgressBar>
            </div>
          </div>

          <div className='userInfo'>
            <div>About Me</div>
            <textarea
              className='inputUserInfo'
              placeholder='No user info has been entered yet!'
              name='info'
              maxLength={255}
              disabled={!editable}
              onChange={handleInput}
              defaultValue={userInfo.info ? userInfo.info : ''}></textarea>
          </div>
        </div>
        <div className='right'>
          <Tabs handleTabs={handleTabs}></Tabs>
          {tab === '1' ? (
            <div className='info'>
              <form>
                <div className='info_data'>
                  <div className='data'>
                    <div className='data-title'>
                      <h4>Name</h4>
                    </div>
                    {editable ? (
                      <div className='input_names'>
                        <input
                          type='text'
                          name='firstName'
                          onChange={handleInput}
                          defaultValue={
                            userInfo.firstName ? userInfo.firstName : 'John'
                          }
                        />
                        <input
                          type='text'
                          name='lastName'
                          onChange={handleInput}
                          defaultValue={
                            userInfo.lastName ? userInfo.lastName : 'Doe'
                          }
                        />
                      </div>
                    ) : (
                      <span>
                        {userInfo.firstName && userInfo.lastName
                          ? `${userInfo.firstName} ${userInfo.lastName}`
                          : 'John Doe'}
                      </span>
                    )}
                  </div>
                  <div className='data'>
                    <div className='data-title'>
                      <h4>Email</h4>
                    </div>
                    <div>{userInfo.email}</div>
                  </div>
                  <div className='data'>
                    <div className='data-title'>
                      <h4>Gender</h4>
                    </div>
                    {editable ? (
                      <select
                        name='gender'
                        id='gender'
                        onChange={handleInput}
                        defaultValue={userInfo.gender}>
                        <option value={0}>Not assigned</option>
                        <option value={1}>Male</option>
                        <option value={2}>Female</option>
                        <option value={3}>Other</option>
                      </select>
                    ) : userInfo.gender === gender.NOT_ASSIGNED ? (
                      'Not assigned'
                    ) : userInfo.gender === gender.MALE ? (
                      'Male'
                    ) : userInfo.gender === gender.FEMALE ? (
                      'Female'
                    ) : userInfo.gender === gender.OTHER ? (
                      'Other'
                    ) : (
                      'N/A'
                    )}
                  </div>
                  <div className='data'>
                    <div className='data-title'>
                      <h4>Age</h4>
                    </div>
                    {editable ? (
                      <div className='profile-inputs'>
                        <div className='inputs-with-unit'>
                          <input
                            type='number'
                            name='age'
                            onChange={handleInput}
                            onBlur={validateAge}
                            defaultValue={userInfo.age ? userInfo.age : 'N/A'}
                          />
                          <span>y</span>
                        </div>
                        {ageError && <div className='error'>{ageError}</div>}
                      </div>
                    ) : (
                      <span>{userInfo.age ? userInfo.age + ' y' : 'N/A'}</span>
                    )}
                  </div>
                  <div className='data'>
                    <div className='data-title'>
                      <h4>Height</h4>
                    </div>
                    {editable ? (
                      <div className='profile-inputs'>
                        <div className='inputs-with-unit'>
                          <input
                            type='number'
                            name='height'
                            onChange={handleInput}
                            onBlur={validateHeight}
                            defaultValue={
                              userInfo.height ? userInfo.height : 'N/A'
                            }
                          />
                          <span>cm</span>
                        </div>
                        {heightError && (
                          <div className='error'>{heightError}</div>
                        )}
                      </div>
                    ) : (
                      <span>
                        {userInfo.height ? userInfo.height + ' cm' : 'N/A'}
                      </span>
                    )}
                  </div>
                  <div className='data'>
                    <div className='data-title'>
                      <h4>Weight</h4>
                    </div>
                    {editable ? (
                      <div className='profile-inputs'>
                        <div className='inputs-with-unit'>
                          <input
                            type='number'
                            name='weight'
                            onBlur={validateWeight}
                            defaultValue={
                              userInfo.weight ? userInfo.weight : 'N/A'
                            }
                          />
                          <span>kg</span>
                        </div>
                        {weightError && (
                          <div className='error'>{weightError}</div>
                        )}
                      </div>
                    ) : (
                      <span>
                        {userInfo.weight ? userInfo.weight + ' kg' : 'N/A'}
                      </span>
                    )}
                  </div>
                </div>
              </form>
              {sessionStorage.getItem('uid') === userId && (
                <div className='buttons'>
                  <button
                    onClick={() => {
                      setEditable(!editable);
                      setAgeError('');
                      setHeightError('');
                      setWeightError('');
                    }}>
                    {editable ? 'Cancel' : 'Edit Profile'}
                  </button>
                  <button
                    disabled={ageError || heightError || weightError}
                    hidden={!editable}
                    onClick={handleUpdateProfile}>
                    Update Profile
                  </button>
                </div>
              )}
            </div>
          ) : (
            <>
              <div className='user-history-legend'>
                {Object.entries(activityColorCoding).map((el) => (
                  <Badge key={`ui-${el[1]}`} color={el[1]} text={el[0]} />
                ))}
              </div>
              <div className='profile-history'>
                <UsersCalendar
                  data={userActivities.reduce((p, c) => {
                    return [
                      ...p,
                      {
                        type: c?.activity,
                        date: extractDate(c?.dateActivity.seconds),

                        title: c.title,
                        points: c.points,
                        color: activityColorCoding[c.activity],
                        timestamp: c?.date.seconds,
                      },
                    ];
                  }, [])}
                />
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Profile;
