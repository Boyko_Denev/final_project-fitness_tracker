import './Users.css';
import AppState from '../../providers/app-state';
import { UserPicture } from '../../components/UserPicture/UserPicture';
import { useContext, useEffect, useState } from 'react';
import {
  friendRequestStatus,
  toastOptions,
  userRole,
} from '../../common/constants';
import {
  getAllUsers,
  getUserById,
  updateUserDocumentField,
} from '../../services/users.service';
import { Space, Input, Avatar } from 'antd';
import Tables from '../../components/Table/Table';
import { AddFriend, deleteFriend } from '../../services/friends.service';
import { toast } from 'react-toastify';
import { ReconciliationFilled } from '@ant-design/icons';

const Users = () => {
  const { appState, setState } = useContext(AppState);
  const currentUserRole = sessionStorage.getItem('role');
  const currentUserUid = sessionStorage.getItem('uid');
  const [users, setUsers] = useState([]);
  const [friends, setFriends] = useState(new Map());

  useEffect(() => {
    if (appState.user) {
      const friendsTmp = new Map();
      appState?.friends.forEach((el) => {
        friendsTmp.set(el.sender, el.status);
      });
      setFriends(friendsTmp);
      getAllUsers()
        .then((resp) => {
          setUsers(resp);
        })
        .catch(console.log);
    }
  }, [appState]);
  const { Search } = Input;
  const handleBlock = (userId, status) => {
    updateUserDocumentField('/Users', userId, { isBlocked: !status })
      .then((resp) => {
        getAllUsers()
          .then((data) => setUsers(data))
          .catch(console.log);
      })
      .catch(console.log);
  };

  const handleSearch = (value) => {
    getAllUsers()
      .then((resp) => {
        setUsers(resp.filter((el) => el.userName.includes(value)));
      })
      .catch(console.log);
  };
  const removeFriend = (uid, userName) => {
    deleteFriend(uid, currentUserUid)
      .then(() => {
        deleteFriend(currentUserUid, uid).then(() => {
          toast.success('Friend removed successfully!', toastOptions);
        });
      })
      .catch((e) => {
        console.log(e);
        toast.error('Something went wrong', toastOptions);
      });
  };

  let columns = [
    {
      title: 'Avatar',
      key: 'avatarUrl',
      render: (_, record) => (
        <UserPicture
          avatarUrl={record?.avatarUrl ? record.avatarUrl : null}
          userName={record.userName}
          size='large'
        />
      ),
    },
    {
      title: 'Username',
      dataIndex: 'userName',
      key: 'userName',
    },

    {
      title: 'First Name',
      dataIndex: 'firstName',
      key: 'firstName',
    },
    {
      title: 'Last Name',
      dataIndex: 'lastName',
      key: 'lastName',
    },
    {
      title: 'Level',
      dataIndex: 'level',
      key: 'level',
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
    },

    {
      title: 'Actions',
      key: 'action',
      render: (_, record) =>
        record.uid !== currentUserUid && (
          <Space size='middle'>
            <button
              className={
                friends.get(record.uid) === friendRequestStatus.PENDING
                  ? 'yellow-text'
                  : friends.get(record.uid) === friendRequestStatus.ACCEPTED
                  ? 'red-text'
                  : 'green-text'
              }
              onClick={() => {
                if (friends.has(record.uid)) {
                  removeFriend(record.uid);
                  return;
                }
                AddFriend(
                  currentUserUid,
                  record.uid,
                  friendRequestStatus.PENDING,
                )
                  .then(() => {
                    AddFriend(
                      record.uid,
                      currentUserUid,
                      friendRequestStatus.PENDING,
                      true,
                    ).then(() => {
                      toast.success('Friend request send!', toastOptions);
                    });
                  })
                  .catch((e) => {
                    toast.error('Something went wrong', toastOptions);
                    console.log(e);
                  });
              }}>
              {friends.get(record.uid) === friendRequestStatus.PENDING
                ? 'Cancel request'
                : friends.get(record.uid) === friendRequestStatus.ACCEPTED
                ? 'Remove friend'
                : 'Add friend'}
            </button>
          </Space>
        ),
    },
  ];
  if (currentUserRole == userRole.ADMIN) {
    columns = columns.reduce((p, c, index) => {
      if (index === 2) {
        p.push({ title: 'Email', dataIndex: 'email', key: 'email' });
      }
      if (index === 6) {
        p.push({
          title: 'Role',
          key: 'role',
          render: (_, record) => (
            <span>
              {Object.entries(userRole).find((el) => el[1] === record.role)[0]}
            </span>
          ),
        });
      }
      p.push(c);
      return p;
    }, []);
    columns.push({
      title: 'Admin actions',
      key: 'Admin actions',
      render: (_, record) =>
        record.uid !== currentUserUid && (
          <Space size='middle'>
            <button
              className={record.isBlocked ? 'green-text' : 'red-text'}
              onClick={() => handleBlock(record.uid, record.isBlocked)}>
              {record.isBlocked ? 'Unblock' : 'Block'}
            </button>
          </Space>
        ),
    });
  }
  return (
    <div className='usersContainer'>
      <Search
        placeholder='Search by username'
        allowClear
        onSearch={handleSearch}
        style={{
          width: 300,
        }}
      />
      <Tables
        data={users.sort((a, b) => a?.userName.localeCompare(b?.userName))}
        columns={columns}></Tables>
    </div>
  );
};

export default Users;
