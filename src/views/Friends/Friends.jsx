import { useEffect, useContext, useState } from 'react';
import './Friends.css';
import { getUsersData } from '../../services/users.service';
import AppState from '../../providers/app-state';
import UserCard from '../../components/UserCard/UserCard';
import { friendRequestStatus } from '../../common/constants';

export const Friends = () => {
  const { appState, setState } = useContext(AppState);
  const [friendsData, setFriendsData] = useState([]);

  useEffect(() => {
    if (appState.user) {
      console.log(appState.friends);
      const currentUserFriends = appState.friends.reduce((p, c) => {
        console.log(c.sender);

        return c.status === friendRequestStatus.ACCEPTED ? [...p, c.sender] : p;
      }, []);

      getUsersData(currentUserFriends)
        .then((resp) => {
          setFriendsData(resp);
        })
        .catch(console.log);
    }
  }, [appState]);

  return (
    <div className='friends-container'>
      <div className='friends-list'>
        {friendsData.map((ele) => (
          <UserCard
            key={`userCard-${ele.uid}`}
            uid={ele.uid}
            avatar={ele.avatarUrl ? ele.avatarUrl : null}
            title={ele.userName}
            description={ele.info}
          />
        ))}
      </div>
    </div>
  );
};
