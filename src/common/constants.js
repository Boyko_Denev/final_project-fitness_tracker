export const toastOptions = {
  position: 'top-left',
  autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: false,
  draggable: true,
  progress: undefined,
};

export const userRole = {
  BASIC: 1,
  ADMIN: 2,
  OWNER: 3,
};

export const gender = {
  NOT_ASSIGNED: 0,
  MALE: 1,
  FEMALE: 2,
  OTHER: 3,
};

export const friendRequestStatus = {
  NA: 0,
  PENDING: 1,
  ACCEPTED: 2
}
export const VALIDATION_PASSWORD_LENGTH = 6;

export const VALIDATION_USERNAME_LENGTH = 4;

export const REGEX_PHONE =
  /^(?:(?:\+|00)359[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/;

export const REGEX_EMAIL =
  /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

export const FILTER_UPPERCASE = /(?=.*?[A-Z])/;

export const FILTER_LOWERCASE = /(?=.*?[a-z])/;

export const FILTER_DIGITS = /(?=.*?[0-9])/;

export const FILTER_SPECIAL_CHAR = /(?=.*?[#?!@$%^&*-])/;

export const ALLOWED_IMAGE_EXTENSIONS = /(\.jpg|\.jpeg|\.png)$/i;

export const EIGHT_HOURS = 8 * 60 * 60 * 1000;

export const RECENT_ACTIVITIES_NUMBER = 3;

export const AGE_MIN = 5;

export const AGE_MAX = 150;

export const HEIGHT_MIN = 50;

export const HEIGHT_MAX = 250;

export const WEIGHT_MIN = 20;

export const WEIGHT_MAX = 350;

export const LITER = 1000;

export const WATER_INTAKE_PERIOD = 10

export const LIMIT_SEVEN_DAYS = 7 * 24 * 60 * 60 * 1000;

export const TITLE_MIN_LENGTH = 4;

export const TITLE_MAX_LENGTH = 32;

export const DURATION_MAX_TIME = 24 * 60;

export const MAX_STEPS = 30000;

export const defaultDateFormat = 'DD/MM/YYYY'
export const activityPointsRanges = {
  Walk: [3, 6],
  Run: [8, 20],
  Swim: [5, 14],
  Bike: [6, 16],
  Basketball: [4, 12],
  Football: [4, 12],
  Box: [6, 11],
  Ski: [5, 12],
  Tennis: [5, 12],
  Volleyball: [3, 9],
};

export const activityColorCoding = {
  Walk: 'red',
  Run: 'yellow',
  Swim: 'orange',
  Bike: 'cyan',
  Basketball: 'green',
  Football: 'blue',
  Box: 'purple',
  Ski: 'geekblue',
  Tennis: 'magenta',
  Volleyball: 'volcano',
}
export const weightRange = [40, 150];

export const CHECK_ACTIVITY = ['Walk']

export const TYPE_OF_GOALS = Object.freeze({
  'WEIGHT': 'weight',
  'STEPS': 'steps',
  'POINTS': 'points',
  'WATER': 'water'
})

export const ACTIVITY_TO_GOAL = Object.freeze({
  'walk': { steps: 'totalSteps', points: 'totalPoints', duration: 'totalDuration' },
  'run': { points: 'totalPoints', duration: 'totalDuration' },
  'bike': { points: 'totalPoints', duration: 'totalDuration' },
  'swim': { points: 'totalPoints', duration: 'totalDuration' },
  'basketball': { points: 'totalPoints', duration: 'totalDuration' },
  'football': { points: 'totalPoints', duration: 'totalDuration' },
  'box': { points: 'totalPoints', duration: 'totalDuration' },
  'ski': { points: 'totalPoints', duration: 'totalDuration' },
  'tennis': { points: 'totalPoints', duration: 'totalDuration' },
  'volleyball': { points: 'totalPoints', duration: 'totalDuration' }
})

export const GOAL_MIN_LEN_NAME = 4
export const GOAL_MAX_LEN_NAME = 30
