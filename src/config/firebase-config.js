import { initializeApp } from 'firebase/app';
import { getAuth, GoogleAuthProvider } from "firebase/auth";
import { getStorage } from 'firebase/storage'
import { getFirestore } from 'firebase/firestore'
import { getAnalytics } from 'firebase/analytics'

const firebaseConfig = {
  apiKey: "AIzaSyBcWMqPPa9YJkNcJKhmfe-Sgun6tDQo3wk",
  authDomain: "fitness-tracker-finalproject.firebaseapp.com",
  projectId: "fitness-tracker-finalproject",
  storageBucket: "fitness-tracker-finalproject.appspot.com",
  messagingSenderId: "993214137037",
  appId: "1:993214137037:web:f25d01f247ecc2f5e25f84",
  measurementId: "G-97K9KM457N"
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const storage = getStorage(app)
export const analytics = getAnalytics(app);
export const authProvider1 = new GoogleAuthProvider();
export const db = getFirestore(app);
