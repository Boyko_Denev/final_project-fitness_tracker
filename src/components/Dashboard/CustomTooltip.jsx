export const CustomTooltip=(props)=>{
  const { label, payload, active,day } = props;
  const [totalTime,totalPoints] = payload
  if (!active || !label || payload.length === 0) return null;

  return (
    totalTime.value > 0 && <div className="custom-tooltip">
      <p>{day?'hour':'date'} {label}</p>
      <p>{`total points : ${totalPoints.value}`}</p>
      <p >{`total time : ${totalTime.value}`}</p>
    </div>
  );
}