import { Button } from 'antd';
import './Filters.css'
export const Filters = ({prop}) => {
  const {dateSelection,setDateSelection} = prop

  const handleClick=(val)=>{
    setDateSelection(val)
  }

  return (
    <div className='filterContainer'>
      <Button type={dateSelection==='day' ?'danger':'primary'}  onClick={()=>handleClick('day')} >Day</Button>
      <Button type={dateSelection==='week' ?'danger':'primary'}   onClick={()=>handleClick('week')}>Week</Button>
      <Button type={dateSelection==='month' ?'danger':'primary'}   onClick={()=>handleClick('month')} >Month</Button>
    </div>
  )
}