/* eslint-disable default-case */
import { useEffect, useState } from 'react';
import { SingleChart } from './SingleChart.jsx';
import { getAgregateDataLast24h, getData } from './helpers.js';

export const ActivityCharts = ({ prop }) => {
  const [chartData, setChartData] = useState([]);
  const { userActivities, dateSelection, selectedActivities, setData } = prop;
  const [day, setDay] = useState(false);
  let data = [];

  useEffect(() => {
    switch (dateSelection) {
      case 'day':
        data = getAgregateDataLast24h(1, userActivities, selectedActivities);
        setChartData(data);
        setData(data);
        setDay(true);
        break;
      case 'week':
        data = getData(7, userActivities, selectedActivities);
        setChartData(data);
        setData(data);
        setDay(false);
        break;
      case 'month':
        data = getData(30, userActivities, selectedActivities);
        setChartData(data);
        setData(data);
        setDay(false);
        break;
    }
  }, [dateSelection, selectedActivities]);

  return (
    <>
      <div>ActivityChart</div>
      <div className='chart-container'>
        <SingleChart prop={{ chartData, day }} />
      </div>
    </>
  );
};
