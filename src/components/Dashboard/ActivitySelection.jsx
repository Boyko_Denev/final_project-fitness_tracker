import {Select} from 'antd';
import {useEffect, useState} from 'react';
const {Option} = Select;

export const ActivitySelection = ({prop}) => {
  const {activityData, selectedActivities,setSelectedActivities} = prop;
  const [activities, setActivities] = useState([]);
  const [alert,setAlert] = useState('none')

  const handleChange=(value,option)=>{
    setSelectedActivities(option.map(obj=>obj.children))
  }

  useEffect(()=>{
    if(selectedActivities.length === 0){
      setAlert('2px solid red')
    }else{
      setAlert('none')
    }
  },[selectedActivities])

  useEffect(() => {
      const tempObj = {};
    activityData && activityData.map((activity) => {
      const {uid,activity:name} = activity
          tempObj[name] = <Option key={uid}>{name}</Option>;
        }
      );
      setActivities(Object.values(tempObj))
  }, [activityData]);

  return (
    <div style={{width:'33vw',minWidth:290}}>
      <Select
        showArrow={true}
        allowClear={true}
        mode="multiple"
        style={{width: '100%',border: alert,fontSize:'1.2em'}}
        placeholder="Please select"
        maxTagCount={3}
        maxTagTextLength={4}
        onChange={handleChange}
        loading={activityData ? false:true}
      >
        {activities}
      </Select>
    </div>
  );
};