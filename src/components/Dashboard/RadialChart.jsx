import { useState,useEffect } from "react";
import { RadialBarChart, RadialBar, PolarAngleAxis } from "recharts";
export const RadialChart=({data})=>{
const [loader,setLoaded]= useState(false)
const circleSize = 200
useEffect(()=>{if(data?.target){
    setLoaded(true)
}},[data])
    return (loader?
    
    <RadialBarChart
  width={circleSize}
  height={circleSize}
  cx={circleSize / 2}
  cy={circleSize / 2}
  innerRadius={80}
  outerRadius={200}
  barSize={20}
  data={[data]}
  startAngle={90}
  endAngle={-270}
  >
  <PolarAngleAxis
  type="number"
  domain={[0, data.target ]}
  angleAxisId={0}
  tick={false}
  />
  <RadialBar
  background
  clockWise
  
  dataKey="value"
  fill="#82ca9d"
  />
  <text
  x={circleSize / 2}
  y={circleSize / 2}
  textAnchor="middle"
  dominantBaseline="middle"
  className="progress-label"
  >
    {`${data.value}/${data.target}`}
  </text>
</RadialBarChart>:
<h3>No goal at the moment!</h3>
)
}