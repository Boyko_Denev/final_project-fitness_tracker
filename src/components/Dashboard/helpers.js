import {activitiesData} from '../../data/mockActivities.js';
import moment from 'moment';

const getAggregateByDay = (day, data, selectedActivities) => {
  /* Gett all metrics for day and agregate then */
  /* returns sum off all points for whole day */
  let totalTime = 0;
  let totalPoints = 0;
  const startDay = moment().startOf('day').subtract(day - 1, 'days').toDate().valueOf();
  const endDay = moment().endOf('day').subtract(day - 1, 'days').toDate().valueOf();
  data && data.forEach((activity) => {
    const {points,dateActivity,activity:name,duration:durationMin} = activity

    const startTime= dateActivity.toDate().valueOf()
    if (startTime > startDay && startTime < endDay && selectedActivities.includes(name)) {
      totalPoints += points;
      totalTime += durationMin;
    }
  });

  return {
    totalPoints,
    totalTime,
    day: moment(startDay).format('DD/MM')
  };
};

const getLast24hActivities = (day, data, selectedActivities) => {
  /* Gett all metrics for last 24h and return them */
  /* returns sum off all metrics for whole day */
  const result = [];
  const startDay = moment().startOf('day').toDate().valueOf();
  const endDay = moment().endOf('day').toDate().valueOf();

  data && data.forEach((activity) => {
    const {points,dateActivity,activity:name,duration:durationMin} = activity
    const startTime= dateActivity.toDate().valueOf()
    if (startTime > startDay && startTime < endDay && selectedActivities.includes(name)) {
      result.push(activity);
    }
  });

  return result;
};

export const getAgregateDataLast24h = (day, data, selectedActivities) => {
  const last24hActivities = getLast24hActivities(day, data, selectedActivities);
  const startDay = moment().startOf('day').toDate().valueOf();
  const hours24 = Array.from(Array(24).keys()).map((val) => moment(startDay).add(val, 'hour'));

  return hours24.reduce((acc, curHour) => {
    let totalTime = 0;
    let totalPoints = 0;

    last24hActivities.forEach(activity => {
      const {points,dateActivity,activity:name,duration:durationMin} = activity
      const startTime= dateActivity.toDate().valueOf()
      if (
        startTime > curHour.valueOf() &&
        startTime < curHour.endOf('hour').valueOf()
      ) {
        totalPoints += points;
        totalTime += durationMin;
      }
    });

    acc.push({
      totalPoints,
      totalTime,
      day: `${curHour.format('H')}h`
    });

    return acc;
  }, []);
};

export const getData = (days, data, selectedActivities) => {
  const selectedData = [];
  getAgregateDataLast24h(days, data, selectedActivities);

  for (let i = days; i >= 0; i--) {
    selectedData.push(getAggregateByDay(i, data, selectedActivities));
  }

  return selectedData;
};

