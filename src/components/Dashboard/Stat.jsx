import { Card, Statistic } from 'antd';

export const Stat = ({ data, suffix, title }) => {
  return (
    <Card>
      <Statistic
        title={title}
        value={data}
        valueStyle={{
          color: '#3f8600',
          width: '272px',
        }}
        suffix={suffix}
      />
    </Card>
  );
};
