import {
  Bar,
  Label,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
  ReferenceLine,
} from 'recharts';
import React from 'react';
import { CustomTooltip } from './CustomTooltip.jsx';

export const SingleChart = ({ prop }) => {
  const { chartData, day } = prop;

  return (
    <div style={{ width: '97%', height: '300px' }}>
      <ResponsiveContainer width='90%' height='100%'>
        <BarChart width={100} data={chartData} barGap={5}>
          <CartesianGrid strokeDasharray='3 3' />
          <XAxis
            stroke='blue'
            strokeWidth={2}
            dataKey={'day'}
            tick={{ fill: '#f0f7ff', fontSize: '0.7em' }}
          />
          <YAxis
            stroke='red'
            strokeWidth={2}
            tick={{ fontSize: '0.7em', fill: '#f0f7ff' }}
          />
          <Tooltip content={<CustomTooltip day={day} />} />
          <ReferenceLine
            y={800}
            label={{
              value: 'Healthy Line',
              fill: 'white',
              position: 'insideTopRight',
              fontSize: '1em',
              backgroundColor: 'green',
            }}
            strokeWidth={5}
            stroke={'#ec052f'}
            ifOverflow='extendDomain'
          />
          <Bar
            dataKey='totalTime'
            fill='#8884d8'
            name='Total time'
            barSize={10}
            maxBarSize={20}
          />
          <Bar
            dataKey='totalPoints'
            fill='#82ca9d'
            name='Total points'
            barSize={10}
            maxBarSize={20}
          />
          <Legend verticalAlign='bottom' height={36} />
        </BarChart>
      </ResponsiveContainer>
    </div>
  );
};
