import {useEffect} from 'react';
import { UserPicture } from '../UserPicture/UserPicture';
import 'antd/dist/antd.css';
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import { Avatar, Card } from 'antd';
import { useNavigate } from 'react-router-dom';

const { Meta } = Card;

const UserCard = ({uid,avatar,title,description,level}) => {
  const navigate = useNavigate();
return(
  
  <Card
    style={{
      width: 300,
    }}
    actions={[
      <p style={{color:'#000'}} onClick={()=>{
        navigate(`/profile/${uid}`)}}>View Profile</p>,
    ]}
  >
    <Meta
      avatar={<UserPicture avatarUrl={avatar} userName={title} size='large'/>
    }
      title={title}
      description={description}
    />
  </Card>
);
  }
export default UserCard;