import { useState } from 'react';
import { Badge, Calendar } from 'antd';
import moment from 'moment';
import './UsersCalendar.css'
export const UsersCalendar = ({data}) => {
  
    
    const getListData = (value) => {
     const date = value.format('DD/MM/YYYY')
        return  data.filter(el=>el.date===date);
      };
        const dateCellRender = (value) => {
          const listData = getListData(value);
          return (
            <div className="events">
              {listData.map((item) => (
                
              <Badge key={item.timestamp.toString()} color={item.color} text={item.title} />
                
              ))}
            </div>
          );
        };
return (<Calendar  
mode = 'month'
validRange = {[moment('01/01/2022'),moment('12/31/2023')]}
dateCellRender={dateCellRender}/>)

    }