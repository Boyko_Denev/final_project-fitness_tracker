import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faBasketball} from '@fortawesome/free-solid-svg-icons';
import './activity.css';

export const Basketball = () => {
  return (
    <div className="activityContainer">
      <div>
        <div>
          <FontAwesomeIcon icon={faBasketball} className="mainAvatar"/> <span>time</span>
          <h2> Basketball </h2>
          <p> duration</p>
        </div>
        <p className="activityDate">Дата</p>
      </div>
      <div style={{
        backgroundColor: "rgba(255,255,255,0.1)",
        color: "black",
        textShadow: " 2px 2px 3px rgba(255,255,255,0.5)",
        flexBasis: "80%"
      }}>Test 123
      </div>
      <div className="activityAvatar"><FontAwesomeIcon icon={faBasketball}/></div>
    </div>
  );
};