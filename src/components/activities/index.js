
import {useEffect, useState} from 'react';
import {Bike} from './bike.jsx';
import {Basketball} from './basketball.jsx';
import {Swiming} from './swiming.jsx';

export const Activity=(prop)=>{
  const {name} = prop.data
  const [sport,setSport] = useState(null)
  console.log(name)
  useEffect(()=> {
    switch (name) {
      case 'basketball':
        setSport(<Basketball/>)
        break
      case 'swiming':
        setSport(<Swiming />)
        break
      case 'bike':
        setSport(<Bike />)
        break
    }
  },[])
  return (
    <div>{sport}</div>
  )
}