import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSwimmer} from '@fortawesome/free-solid-svg-icons';
import './activity.css';

export const Swiming = () => {
  return (
    <div className="activityContainer">
      <div>
        <div>
          <FontAwesomeIcon icon={faSwimmer} className="mainAvatar"/> <span>time</span>
          <h2> Swiming </h2>
          <p> duration</p>
        </div>
        <p className="activityDate">Дата</p>
      </div>

      <div className="activityAvatar"><FontAwesomeIcon icon={faSwimmer}/></div>
    </div>
  );
};