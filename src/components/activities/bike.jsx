import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faBiking} from '@fortawesome/free-solid-svg-icons';
import './activity.css';

export const Bike = () => {
  return (
    <div className="activityContainer">
      <div>
        <div>
          <FontAwesomeIcon icon={faBiking} className="mainAvatar"/> <span>time</span>
          <h2> Biking </h2>
          <p> duration</p>
        </div>
        <p className="activityDate">Дата</p>
      </div>

      <div className="activityAvatar"><FontAwesomeIcon icon={faBiking}/></div>
    </div>
  );
};