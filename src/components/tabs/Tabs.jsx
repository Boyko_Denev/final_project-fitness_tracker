import { useState } from 'react';
import '../tabs/Tabs.css'
const Tabs = ({handleTabs}) => {
const [currentTab, setCurrentTab] = useState('1');

    const tabs = [
        {
            id: 1,
            tabTitle: 'Info',
            content: ''
        },
        {
            id: 2,
            tabTitle: 'History',
            content: ''
        },
    ];

    const handleTabClick = (e) => {
        setCurrentTab(e.target.id);
        handleTabs(e.target.id)
    }

    return (
        <div className='containerTabs'>
            <div className='tabs'>
                {tabs.map((tab, i) =>
                    <button key={i} id={tab.id} disabled={currentTab === `${tab.id}`} onClick={(handleTabClick)}>{tab.tabTitle}</button>
                )}
            </div>
        </div>
    );
}
export default Tabs