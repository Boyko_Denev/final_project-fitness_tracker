import React, { useEffect, useState } from 'react';
import 'antd/dist/antd.min.css';
import { Table } from 'antd';

const Tables = ({ data, columns }) => {
  const [pagination, setPagination] = useState({});

  useEffect(() => {
    setPagination({
      pageSize: 5,
      total: data.length,
      showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
    });
  }, [data]);

  return (
    <Table
      rowKey={(record) => record.uid}
      columns={columns}
      dataSource={data}
      pagination={pagination}
    />
  );
};

export default Tables;
