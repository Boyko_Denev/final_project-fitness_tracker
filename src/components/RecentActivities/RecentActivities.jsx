import './RecentActivities.css';

import React, { useEffect, useState } from 'react';
import { getRecentActivities } from '../../services/activities.service';
import ClipLoader from 'react-spinners/ClipLoader';
import { toast } from 'react-toastify';
import { toastOptions } from '../../common/constants';

const RecentActivities = () => {
  const userId = sessionStorage.getItem('uid');
  const [dataActivities, setDataActivities] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getRecentActivities(userId)
      .then((resp) => {
        setDataActivities(resp);
        setLoading(false);
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  }, []);

  return (
    <>
      {loading ? (
        <div className='Spinner'>
          <ClipLoader
            color={'rgb(0, 45, 41)'}
            loading={loading}
            size={100}
            speedMultiplier='0.7'
          />
        </div>
      ) : (
        <div className='recent-container'>
          <h2>Recent Activities</h2>
          {dataActivities.map((activity) => {
            return (
              <div key={activity.docId} className='recent-activity'>
                <hr className='line1'></hr>
                <div className='activity-date'>
                  <div className='activity'>{activity.activity}</div>
                  <div className='date'>
                    {activity.dateActivity.toDate().toLocaleDateString('en-GB')}
                  </div>
                </div>
                <div className='title'>"{activity.title}"</div>
                <div className='points'>
                  <div className='value'>{activity.points}</div>
                  <div className='pts'>pts</div>
                </div>
              </div>
            );
          })}
        </div>
      )}
    </>
  );
};

export default RecentActivities;
