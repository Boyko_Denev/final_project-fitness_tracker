import './WaterDailyChart.css';

import React, { useEffect, useState } from 'react';
import { getWaterIntakePerDay } from '../../services/water.service';
import {
  Bar,
  Label,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
  ReferenceLine,
} from 'recharts';
import waterLimitDaily from '../../utils/waterCalculator';
import { getUserWeight } from '../../services/users.service';
import { toastOptions } from '../../common/constants';
import { toast } from 'react-toastify';
import { WATER_INTAKE_PERIOD } from '../../common/constants';
import ClipLoader from 'react-spinners/ClipLoader';

const WaterDailyChart = () => {
  const userId = sessionStorage.getItem('uid');
  const [dataWater, setDataWater] = useState([]);
  const [waterLimit, setWaterLimit] = useState(0);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getUserWeight(userId)
      .then((res) => {
        setWaterLimit(waterLimitDaily(res));
        const tempArr = [];
        for (let i = WATER_INTAKE_PERIOD - 1; i >= 0; --i) {
          getWaterIntakePerDay(userId, i)
            .then((data) => {
              tempArr.push(data);
            })
            .catch((err) => {
              toast.error(err.message, toastOptions);
            });
        }
        setLoading(false);
        setDataWater(tempArr);
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  }, []);

  return (
    <>
      {loading ? (
        <div className='Spinner'>
          <ClipLoader
            color={'rgb(0, 45, 41)'}
            loading={loading}
            size={100}
            speedMultiplier='0.7'
          />
        </div>
      ) : (
        <div className='water-intake-container'>
          <div className='water-intake-char'>
            <ResponsiveContainer width='100%' height='100%'>
              <BarChart
                // width={500}
                // height={300}
                data={dataWater}
                margin={{
                  top: 5,
                  right: 30,
                  left: 20,
                  bottom: 5,
                }}>
                <CartesianGrid strokeDasharray='3 3' />
                <XAxis dataKey='date' />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey='Water' fill='#2f9bff' />
                <ReferenceLine
                  y={waterLimit * 1000}
                  label={{
                    value: 'Water Intake Limit',
                    position: 'insideTopRight',
                    fontSize: '1em',
                  }}
                  strokeWidth={3}
                  stroke={'green'}
                  ifOverflow='extendDomain'
                />
              </BarChart>
            </ResponsiveContainer>
          </div>
        </div>
      )}
    </>
  );
};

export default WaterDailyChart;
