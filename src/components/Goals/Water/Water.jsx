import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCircleChevronUp} from '@fortawesome/free-solid-svg-icons';

export const Water = ({close}) => {
  return (
    <>
      <FontAwesomeIcon icon={faCircleChevronUp} onClick={()=>{
        window.scrollTo({top: 0, behavior: 'smooth'})
        setTimeout(()=>close(false),700)
      }} />
      <h1>water</h1>
    </>
  )
}