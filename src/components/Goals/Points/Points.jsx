import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCircleChevronUp, faMinus, faPlus} from '@fortawesome/free-solid-svg-icons';
import {Button, DatePicker, InputNumber, Space, Switch,Form} from 'antd';
import moment from 'moment';
import {useEffect, useState} from 'react';
import {addGoalToDb, checkIfGoalExists, switchGoal} from '../../../services/goals.service.js';
import ClipLoader from 'react-spinners/ClipLoader';
import {GOAL_MAX_LEN_NAME, GOAL_MIN_LEN_NAME, TYPE_OF_GOALS} from '../../../common/constants.js';
import Input from 'antd/es/input/Input.js';
import {getDoc} from 'firebase/firestore';

const {RangePicker} = DatePicker;

const disabledDate = (current) => {
  return current && current < moment().startOf('day');
};

export const Points = ({close}) => {
  const [target, setTarget] = useState(0);
  const [toggle, setToggle] = useState(false);
  const [datePick, setDatePick] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const [name,setName] = useState('')
  const [nameError,setNameError] = useState(false)
  const [range,setRange] = useState(true)
  const handleSubmitData = (name,uid, start, end, target) => {
    setIsLoading(true);
    return addGoalToDb(name,sessionStorage.uid, start, end, target, TYPE_OF_GOALS.POINTS).then(response => {
      setIsLoading(false);
      setRedirect(true);
    });
  };

  const getGoalRecord = (uid, type) => {
    checkIfGoalExists(uid, type).then(result => {
      if (result) {
        getDoc(result).then(goal=>{
          const {start,end,target,name,totalPoints} = goal.data()
          if(name) {
            setTarget(target ?? 0);
            setName(name)
            setDatePick([moment(start.toDate().valueOf()), moment(end.toDate().valueOf())])
          }
        })
        setToggle(true);

      }
    });
  };

  useEffect(()=>{
    name?.length >0 && (name.length < GOAL_MIN_LEN_NAME || name.length > GOAL_MAX_LEN_NAME) ? setNameError(true):setNameError(false)
  },[name])
  useEffect(() => {
    getGoalRecord(sessionStorage.uid, TYPE_OF_GOALS.POINTS);
  }, []);

  useEffect(()=>{
    if(datePick?.length > 0){
      setRange(false)
    }
  },[datePick])
  const redirectOnSuccess = () => {
    return (
      <div> Saved sucessfuly </div>
    );
  };

  const minusPoints = () => {
    datePick.length > 0 && setTarget((val) =>
      val - 1 < 0 ? 0 : val - 1
    );
  };
  const plusPoints = () => {
    datePick.length > 0 && setTarget((val) => val + 1);
  };

  const onToggle = (checked) => {
    switchGoal(sessionStorage.uid, TYPE_OF_GOALS.POINTS);
    setToggle(!toggle);
    setRedirect(false)
    setDatePick([]);
    setName('')
    setTarget(0)
    setRange(true)
  };

  const renderLoader = () => {
    return (
      <ClipLoader/>
    );
  };
  const renderBody = () => {
    return (
      <>
        <div className="goalFormHLine">
          <label>Name your goal:
          <br />
            <Form.Item
              className='inputGoalField'
              onChange={(e)=>{
                setName(e.target.value)
              }}
              validateStatus={nameError ?"error":''}
              help={nameError && "Name must be unique and between 4 and 30 symbols"}
            >
              <Input placeholder="Name" id="error" value={name}/>
            </Form.Item>
          </label>
        </div>
        <div className="goalFormHLine">
          <label>Please select date range:
            <br/>
            <Space direction="vertical" size={12} className="formWidth">
              <RangePicker format="YYYY-MM-DD"
                           disabled={nameError || name?.length === 0}
                           className="formWidth"
                           allowClear={false}
                           style={{width: '100%'}}
                           value={datePick}
                           disabledDate={disabledDate}
                           // value={datePick}
                           onChange={(dp) => {
                             dp ? setDatePick([...dp]) : setDatePick([]);
                           }}
              />
            </Space>
          </label>
        </div>
        <div className="goalFormHLine">

          <label>Points to reach:<br/>
            <Space direction="vertical" className="formWidth">
              <InputNumber style={{width: '100%'}}

                           addonBefore={
                             <div style={{width: '3vw', fontSize: '1rem', fontWeight: 'bolder'}}
                                  onClick={minusPoints}><FontAwesomeIcon icon={faMinus}/></div>
                           }
                           disabled={range}
                           addonAfter={<div style={{width: '3vw', fontSize: '1rem', fontWeight: 'bolder'}}
                                            onClick={plusPoints}><FontAwesomeIcon icon={faPlus}/></div>}
                           onChange={(e) => {
                             setTarget(e);
                           }} value={target} defaultValue={target}/>

            </Space>
          </label>
        </div>
        <div className="goalFormHLine"><Button disabled={!(target > 0 && (datePick?.length > 0))}
                                               onClick={() => handleSubmitData(name,sessionStorage.uid, datePick[0].toDate(), datePick[1].toDate(), target)}
                                               className={'goalSubmitButton'}>Submit</Button>

        </div>
      </>
    );
  };

  return (
    <div className="goalContainerForm">
      <FontAwesomeIcon icon={faCircleChevronUp} onClick={() => {
        setDatePick()
        window.scrollTo({top: 0, behavior: 'smooth'});
        setTimeout(() => close(), 700);
      }}/>
      <h1>Points</h1>
      <hr/>
      <div className="goalFormY">
        <div className="goalFormH"><p>Goal is {toggle ? 'on' : 'off'}:</p><Switch checked={toggle}  checkedChildren={<div className='toggleText'><b>Delete</b> goal</div>} unCheckedChildren={'Create goal'}  onChange={onToggle}/>
        </div>
        {toggle && (isLoading ? renderLoader() : (redirect ? redirectOnSuccess() : renderBody()))}

      </div>

    </div>
  );
};

