import './Avatar.css'
export const Avatar=({style,icon,onClick,href,size})=>{
  return (
    <div className='containerAvatar' style={{...style,width:'20vw',height:'20vh',borderRadius: '50px'}} onClick={onClick}>
       <img src={icon}  style={{...style,width:'20vw',height:'20vh',borderRadius: '50px'}}/>
    </div>
  )
}