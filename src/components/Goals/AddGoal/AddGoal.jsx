/* eslint-disable default-case */
import weight from '../../../assets/images/weight-loss-measuring.png';
import { Weight } from '../Weight/Weight.jsx';
import { Steps } from '../Steps/Steps.jsx';
import { Points } from '../Points/Points.jsx';
import { Water } from '../Water/Water.jsx';
import { TYPE_OF_GOALS } from '../../../common/constants.js';

export const AddGoal = ({ props }) => {
  const { setOpenGoal, typeOfGoal } = props;
  const returnGoal = () => {
    switch (typeOfGoal) {
      case TYPE_OF_GOALS.WEIGHT:
        return <Weight close={setOpenGoal} />;
        break;
      case TYPE_OF_GOALS.STEPS:
        return <Steps close={setOpenGoal} />;
      case TYPE_OF_GOALS.POINTS:
        return <Points close={setOpenGoal} />;
        break;
      case TYPE_OF_GOALS.WATER:
        return <Water close={setOpenGoal} />;
    }
  };
  return (
    <div className='addGoalContainer' id='addGoal'>
      {returnGoal()}
    </div>
  );
};
