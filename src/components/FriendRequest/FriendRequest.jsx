import { useEffect, useState } from 'react';
import { getUserData } from '../../services/users.service';
import { Avatar } from 'antd';
import './FriendRequest.css';
import { UserPicture } from '../UserPicture/UserPicture';

export const FriendRequest = ({ userId, handleAccept, handleDecline }) => {
  const [user, setUser] = useState({});
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    getUserData('/Users', userId)
      .then((resp) => {
        setUser(resp.data());
        setLoaded(true);
      })
      .catch(console.log);
  }, []);

  return (
    <div>
      {loaded && (
        <div className='friendRequest-component'>
          <div className='friendRequest-userInfo'>
            <UserPicture
              avatarUrl={user?.avatarUrl ? user?.avatarUrl : null}
              userName={user?.userName}
              size='large'
            />
            <span style={{ marginTop: '8px' }}>{user?.userName}</span>
          </div>
          <div className='friendRequest-buttons'>
            <button id={userId} className='green-text' onClick={handleAccept}>
              Accept
            </button>
            <button id={userId} className='red-text' onClick={handleDecline}>
              Decline
            </button>
          </div>
        </div>
      )}
    </div>
  );
};
