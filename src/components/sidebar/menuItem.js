import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faChartLine,
  faBullseye,
  faPlus,
  faUserGroup,
  faComment,
  faGlassWater,
  faDroplet,
} from '@fortawesome/free-solid-svg-icons'

export const menuItem = [
  {
    title: 'Board',
    path: '/board',
    name: 'nav-text',
    icon: <FontAwesomeIcon icon={faChartLine} />,
  },
 
  {
    title: 'Set Goals',
    path: '/set-goals',
    name: 'nav-text',
    icon: <FontAwesomeIcon icon={faBullseye} />,
  },
  {
    title: 'Add Activities',
    path: '/add-activities',
    name: 'nav-text',
    icon: <FontAwesomeIcon icon={faPlus} />,
  },
  {
    title: 'Friends',
    path: '/friends',
    name: 'nav-text',
    icon: <FontAwesomeIcon icon={faUserGroup} />,

  },
  {
    title: 'Support',
    path: '/support',
    name: 'nav-text',
    icon: <FontAwesomeIcon icon={faComment} />,
  }
]