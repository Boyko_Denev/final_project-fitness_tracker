import './Sidebar.css';
import React, { useContext, useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { UserPicture } from '../UserPicture/UserPicture';
import {
  faBurger,
  faUser,
  faUsers,
  faRightFromBracket,
  faBell,
  faUserGroup,
} from '@fortawesome/free-solid-svg-icons';
import { Badge } from 'antd';
import { NavLink, useNavigate } from 'react-router-dom';
import { menuItem } from './menuItem';
import { toast } from 'react-toastify';
import { toastOptions, friendRequestStatus } from '../../common/constants';
import { getUserData } from '../../services/users.service';
import {
  updateFriendFields,
  deleteFriend,
  getAllFriends,
  AddFriend,
} from '../../services/friends.service';
import AppState from '../../providers/app-state';
import { onSnapshot, collection } from 'firebase/firestore';
import { db } from '../../config/firebase-config';
import { FriendRequest } from '../FriendRequest/FriendRequest';
import Logo from '../../assets/logo/LogoYellow.svg';

export const Sidebar = ({ children }) => {
  const { appState, setState } = useContext(AppState);
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const isUser = sessionStorage.getItem('uid');
  const currentUserName = sessionStorage.getItem('userName');
  const userAvatar = sessionStorage.getItem('avatarUrl');
  const [notificationsIsVisible, setNotificationsIsVisible] = useState(false);
  const [notifications, setNotifications] = useState([]);
  const [friendRequests, setFriendRequests] = useState([]);

  useEffect(() => {
    const unsubscribe = onSnapshot(
      collection(db, `/Users/${isUser}/friends`),
      (snapshot) => {
        const friendList = snapshot.docs.map((doc) => doc.data());
        setState((prev) => {
          return { ...prev, friends: friendList };
        });
        setFriendRequests(
          friendList.filter(
            (el) => !el.isOrigin && el.status === friendRequestStatus.PENDING,
          ),
        );
      },
    );

    return () => {
      unsubscribe();
    };
  }, []);

  useEffect(() => {
    if (isUser) {
      getUserData('/Users', isUser)
        .then((data) => {
          getAllFriends(isUser)
            .then((friends) => {
              setState({
                user: isUser,
                userData: { ...data.data() },
                friends: friends,
              });
            })
            .catch((err) => {
              console.log(err);
              toast.error('Something went wrong!', toastOptions);
            });
        })
        .catch((err) => {
          console.log(err);
          toast.error('Something went wrong!', toastOptions);
        });
    }
  }, [isUser]);

  const handleAccept = (e) => {
    updateFriendFields(e.target.id, isUser, {
      status: friendRequestStatus.ACCEPTED,
    })
      .then(() => {
        updateFriendFields(isUser, e.target.id, {
          status: friendRequestStatus.ACCEPTED,
        });
      })
      .then(() => {
        AddFriend(isUser, e.target.id, friendRequestStatus.ACCEPTED);
      })
      .catch((error) => {
        toast.error('Something went wrong', toastOptions);
        console.log(error);
      });
  };
  const handleDecline = (e) => {
    deleteFriend(e.target.id, isUser)
      .then(() => {
        deleteFriend(isUser, e.target.id);
      })
      .catch(console.error);
  };

  return (
    <div className='s-container'>
      {isUser ? (
        <div className='sidebar' style={{ width: isOpen ? '250px' : '54px' }}>
          <div className='sidebar-box'>
            <div className='sidebar-bar'>
              <FontAwesomeIcon icon={faBurger} onClick={toggle} />
            </div>
            {menuItem.map((item, index) => {
              return (
                <NavLink
                  className='link'
                  to={item.path}
                  key={index}
                  activeclassname='active'>
                  <div className='link-icon'>{item.icon}</div>
                  <div
                    className='link-text'
                    style={{ display: isOpen ? 'block' : 'none' }}>
                    <div className='text-inside'>{item.title}</div>
                  </div>
                </NavLink>
              );
            })}
            <hr className='link-line1' />
            <NavLink className='link' to={'/users'} activeclassname='active'>
              <div className='link-icon'>
                <FontAwesomeIcon icon={faUsers} />
              </div>
              <div
                className='link-text'
                style={{ display: isOpen ? 'block' : 'none' }}>
                <div className='text-inside'>Users</div>
              </div>
            </NavLink>
            <NavLink
              className='link'
              to={`/profile/${isUser}`}
              activeclassname='active'>
              <div className='link-icon'>
                <FontAwesomeIcon icon={faUser} />
              </div>
              <div
                className='link-text'
                style={{ display: isOpen ? 'block' : 'none' }}>
                <div className='text-inside'>Profile</div>
              </div>
            </NavLink>
            <NavLink className='link' to={'/logout'} activeclassname='active'>
              <div className='link-icon'>
                <FontAwesomeIcon icon={faRightFromBracket} />
              </div>
              <div
                className='link-text'
                style={{ display: isOpen ? 'block' : 'none' }}>
                <div className='text-inside'>Log Out</div>
              </div>
            </NavLink>
          </div>
        </div>
      ) : (
        <></>
      )}
      <main>
        <div className='menu-bar'>
          {isUser && (
            <div className='userTopBar'>
              <div
                className='menu-about-us'
                onClick={() => navigate('/about-us')}>
                About Us
              </div>
              <div className='user-icons-topBar'>
                <a href='#' className='userBadges'>
                  <Badge
                    size='small'
                    onClick={() => {
                      setNotificationsIsVisible((p) => !p);
                    }}
                    count={friendRequests.length}>
                    <FontAwesomeIcon
                      fontSize='22px'
                      color='rgb(226, 185, 53)'
                      icon={faUserGroup}
                    />
                  </Badge>
                </a>
                <Badge className='userBadges' count={notifications.length}>
                  <FontAwesomeIcon
                    fontSize='22px'
                    color='rgb(226, 185, 53)'
                    icon={faBell}
                  />
                </Badge>
                <UserPicture
                  avatarUrl={userAvatar ? userAvatar : null}
                  userName={currentUserName}
                />
              </div>
            </div>
          )}
          {isUser ? (
            <img
              className='menu-logo-left'
              onClick={() => navigate('/board')}
              src={Logo}
              alt='logo'></img>
          ) : (
            <img
              className='menu-logo-center'
              onClick={() => navigate('/')}
              src={Logo}
              alt='logo'></img>
          )}
        </div>
        {notificationsIsVisible && (
          <div className='notifications-panel'>
            {friendRequests.length ? (
              friendRequests.map((el, index) => (
                <FriendRequest
                  key={index + el?.sender}
                  userId={el?.sender}
                  handleAccept={handleAccept}
                  handleDecline={handleDecline}
                />
              ))
            ) : (
              <span>No pending friend requests! </span>
            )}
          </div>
        )}

        <div className='main-container'>
          <>{children}</>
        </div>
      </main>
    </div>
  );
};
