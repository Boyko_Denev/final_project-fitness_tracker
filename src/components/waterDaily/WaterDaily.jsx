import './WaterDaily.css';
import React, { useEffect, useState } from 'react';
import { AddWater, GetWaterPresentDay } from '../../services/water.service';
import { getUserWeight } from '../../services/users.service';
import waterLimitDaily from '../../utils/waterCalculator';
import { iconsPlusMinus, listFastWater } from './waterImages';
import { toast, ToastContainer } from 'react-toastify';
import { LITER, toastOptions } from '../../common/constants';
import ClipLoader from 'react-spinners/ClipLoader';

export const WaterDaily = () => {
  const [currentWater, setCurrentWater] = useState(0);
  const [edit, setEdit] = useState(true);
  const [waterLimit, setWaterLimit] = useState(0);
  const [loading, setLoading] = useState(false);
  const userId = sessionStorage.getItem('uid');

  useEffect(() => {
    setLoading(true);

    getUserWeight(userId)
      .then((res) => {
        setWaterLimit(waterLimitDaily(res));

        GetWaterPresentDay(userId)
          .then((res) => {
            setCurrentWater(res);
            setLoading(false);
          })
          .catch((err) => {
            toast.error(err.message, toastOptions);
          });
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  }, []);

  const handleDailyWaterChange = (volume) => {
    AddWater(Number(volume), userId)
      .then(() => {
        setCurrentWater((prev) => prev + volume);
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  };

  const changeEdit = () => {
    if (currentWater === 0) {
      setEdit(true);

      return;
    }
    setEdit(!edit);
  };

  return (
    <>
      {loading ? (
        <div className='Spinner'>
          <ClipLoader
            color={'rgb(0, 45, 41)'}
            loading={loading}
            size={100}
            speedMultiplier='0.7'
          />
        </div>
      ) : (
        <div className='water-container'>
          <div className='water-box'>
            <ToastContainer />
            <div>
              <h2>Water Daily</h2>
            </div>
            <div className='water-daily'>
              <div>
                <div className='water-tube'>
                  <div
                    className='wave'
                    style={{
                      marginTop: `${
                        300 - (0.3 / waterLimit) * currentWater > 0
                          ? 300 - (0.3 / waterLimit) * currentWater
                          : 0
                      }px`,
                    }}></div>
                  <div className='value'>
                    <h3>
                      {currentWater / LITER}
                      <span>
                        <i>liter/s</i>
                      </span>
                    </h3>
                  </div>
                </div>
                <p>
                  Based on your profile, your daily water intake limit should be{' '}
                  <b>{waterLimit.toFixed(1)}</b> liters
                </p>
              </div>
              <div className='fast-water-options'>
                {listFastWater.map(({ id, val, title, img }) => {
                  return (
                    <button
                      disabled={val > currentWater && !edit}
                      className='list-water'
                      key={id}
                      onClick={() =>
                        handleDailyWaterChange(
                          edit ? Number(val) : -Number(val),
                        )
                      }>
                      <div className='icon-text'>
                        <label className='label-water'>
                          {edit ? (
                            <img src={iconsPlusMinus.plus} alt='' />
                          ) : (
                            <img src={iconsPlusMinus.minus} alt='' />
                          )}
                        </label>
                        <div className='list-water-title'>{title}</div>
                      </div>
                      <div className='value-water'>
                        <img src={img} alt='' />
                      </div>
                    </button>
                  );
                })}
                <div className='water-edit'>
                  <button className='edit' onClick={changeEdit}>
                    Edit
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
