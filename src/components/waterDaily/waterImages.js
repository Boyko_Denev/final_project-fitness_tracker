import Plus from '../../assets/water/plus-01.svg'
import Minus from '../../assets/water/minus-01.svg'
import Water250 from '../../assets/water/w-0.25.svg'
import Water500 from '../../assets/water/w-0.5.svg'
import Water1000 from '../../assets/water/w-1.0.svg'
import Water1500 from '../../assets/water/w-1.5.svg'

export const iconsPlusMinus = {
  plus: Plus,
  minus: Minus,
}

export const listFastWater = [
  {
    id: 1,
    val: 250,
    title: '250 mL',
    img: Water250
  },
  {
    id: 2,
    val: 500,
    title: '500 mL',
    img: Water500
  },
  {
    id: 3,
    val: 1000,
    title: '1 L',
    img: Water1000
  },
  {
    id: 4,
    val: 1500,
    title: '1.5 L',
    img: Water1500
  },
]