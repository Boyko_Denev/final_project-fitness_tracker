import { flexbox } from '@mui/system';

const ProgressBar = (props) => {
  const { bgcolor, completed, level, value } = props;

  const containerStyles = {
    height: `calc(22px + 0.2vw)`,
    backgroundColor: '#e0e0de',
    borderRadius: 50,
    width: `calc(130px + 4vw)`,
  };

  const fillerStyles = {
    display: 'flex',
    height: '100%',
    width: `${completed}%`,
    backgroundColor: bgcolor,
    borderRadius: 'inherit',
    paddingRight: '2px',
  };

  const labelStyles = {
    color: 'white',
    fontWeight: '500',
    fontSize: `calc(11px + 0.2vw)`,
  };

  return (
    <div id='progress-bar-wrapper'>
      <h5
        style={{
          color: '#fff',
          fontSize: `calc(16px + 0.4vw)`,
        }}>{`${level} lv.`}</h5>
      <div style={containerStyles}>
        <div style={fillerStyles}></div>
        <span style={labelStyles}>{`${value} pts`}</span>
      </div>
    </div>
  );
};

export default ProgressBar;
