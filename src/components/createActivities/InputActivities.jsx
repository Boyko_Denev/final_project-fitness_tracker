import './InputActivities.css';
import {
  CHECK_ACTIVITY,
  DURATION_MAX_TIME,
  LIMIT_SEVEN_DAYS,
  MAX_STEPS,
  TITLE_MAX_LENGTH,
  TITLE_MIN_LENGTH,
} from '../../common/constants';
import { DatePicker } from 'antd';
import moment from 'moment';

export const InputActivities = (props) => {
  const clearTitleError = () => {
    props.errors.setTitleError('');
  };

  const validateTitle = () => {
    const title = props.inputDate.title;

    if (title.length < TITLE_MIN_LENGTH || title.length > TITLE_MAX_LENGTH) {
      props.errors.setTitleError(
        `Title must be in range ${TITLE_MIN_LENGTH} - ${TITLE_MAX_LENGTH} symbols`,
      );
      return;
    }
  };

  const handleInputTitle = (event) => {
    props.inputDate.setTitle(event.target.value);
  };

  const clearDurationError = () => {
    props.errors.setDurationError('');
  };

  const validateDuration = () => {
    const duration = props.inputDate.duration;

    if (duration < 0 || duration > DURATION_MAX_TIME || duration === '') {
      props.errors.setDurationError(
        `Input valid duration max ${
          DURATION_MAX_TIME / 60
        } (${DURATION_MAX_TIME} min)`,
      );
      return;
    }
  };

  const handleInputDuration = (event) => {
    const duration = event.target.value;
    if (isNaN(duration)) {
      props.errors.setDurationError('Input valid duration');
      return;
    } else {
      props.errors.setDurationError('');
      props.inputDate.setDuration(Number(event.target.value));
    }
  };

  const clearStepsError = () => {
    props.errors.setStepsError('');
  };

  const validateSteps = () => {
    const steps = props.inputDate.steps;

    if (steps < 0 || steps > MAX_STEPS || steps === '') {
      props.errors.setStepsError(
        `Input valid number of steps (max ${MAX_STEPS})`,
      );
      return;
    }
  };

  const handleInputSteps = (event) => {
    const steps = event.target.value;

    if (isNaN(steps)) {
      props.errors.setStepsError('Input valid number of steps');
      return;
    } else {
      props.errors.setStepsError('');
      props.inputDate.setSteps(Number(event.target.value));
    }
  };

  const handleInputDateActivity = (value) => {
    props.inputDate.setDateActivity(value._d);
  };

  const dateFormatList = ['DD/MM/YYYY', 'DD/MM/YY'];

  const DisableDates = (current) => {
    return (
      current &&
      (current < moment().endOf('day') - LIMIT_SEVEN_DAYS ||
        current > moment().endOf('day'))
    );
  };

  return (
    <div className='input-activity'>
      <label>Title</label>
      <input
        className='input-activity-title'
        type='text'
        value={props.inputDate.title}
        onChange={handleInputTitle}
        onBlur={validateTitle}
        onFocus={clearTitleError}></input>
      <div className='input-distance'>
        <div className='error'>{props.errors.titleError}</div>
      </div>
      <label>Duration</label>
      <div className='duration-min'>
        <input
          className='input-activity-duration'
          type='text'
          value={props.inputDate.duration}
          onChange={handleInputDuration}
          onBlur={validateDuration}
          onFocus={clearDurationError}></input>
        <div className='min'>min</div>
      </div>
      <div className='input-distance'>
        <div className='error'>{props.errors.durationError}</div>
      </div>
      {CHECK_ACTIVITY.includes(props.inputDate.activity) ? (
        <>
          <label>Steps</label>
          <div className='steps'>
            <input
              className='input-activity-steps'
              type='text'
              value={props.inputDate.steps}
              onChange={handleInputSteps}
              onBlur={validateSteps}
              onFocus={clearStepsError}></input>
          </div>
          <div className='input-distance'>
            <div className='error'>{props.errors.stepsError}</div>
          </div>
        </>
      ) : (
        <></>
      )}
      <label>Date on Activity</label>
      <div className='input-date-picker'>
        <DatePicker
          defaultValue={moment()}
          format={dateFormatList}
          disabledDate={(current) => DisableDates(current)}
          onChange={handleInputDateActivity}
        />
      </div>
      <div className='input-distance'></div>
    </div>
  );
};
