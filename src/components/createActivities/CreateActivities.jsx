/* eslint-disable default-case */
import './CreateActivities.css';
import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import {
  ACTIVITY_TO_GOAL,
  CHECK_ACTIVITY,
  toastOptions,
} from '../../common/constants';
import {
  getActivitiesByTime,
  getAllActivities,
  setActivitiesPoints,
} from '../../services/activities.service';
import { InputActivities } from './InputActivities';
import activityPoints from '../../utils/activityPointsCalculation';
import {
  getUserData,
  updateUserDocumentField,
} from '../../services/users.service';
import { EditHistoryActivity } from './EditHistoryActivity';
import ClipLoader from 'react-spinners/ClipLoader';
import { serverTimestamp } from 'firebase/firestore';
import { addPoints } from '../../utils/addExperience';
import { checkIfGoalExists, updateGoal } from '../../services/goals.service.js';

export const CreateActivities = () => {
  const [activity, setActivity] = useState('');
  const [activitiesData, setActivitiesData] = useState([]);
  const [title, setTitle] = useState('');
  const [duration, setDuration] = useState('');
  const [steps, setSteps] = useState('');
  const [dateActivity, setDateActivity] = useState(serverTimestamp());
  const [userInfo, setUserInfo] = useState({});
  const [historyActivities, setHistoryActivities] = useState(false);
  const [dataHistory, setDataHistory] = useState([]);
  const [loading, setLoading] = useState(true);
  const [titleError, setTitleError] = useState('');
  const [durationError, setDurationError] = useState('');
  const [stepsError, setStepsError] = useState('');

  const userId = sessionStorage.getItem('uid');

  useEffect(() => {
    getAllActivities()
      .then((resp) => {
        setActivitiesData(resp);
        getUserData('/Users', userId)
          .then((data) => {
            setUserInfo(data.data());
            setLoading(false);
          })
          .catch((err) => {
            toast.error(err.message, toastOptions);
          });
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  }, []);

  const handleClick = (event, activity) => {
    setHistoryActivities(false);
    setActivity(activity);
    setDuration('');
    setTitle('');
    setSteps('');
    setDateActivity(serverTimestamp());
    setTitleError('');
    setDurationError('');
    setStepsError('');
  };

  const Points = () => activityPoints(duration, activity, userInfo?.weight);

  const Submit = () => {
    const points = activityPoints(duration, activity, userInfo?.weight);
    const stepsDate = steps === '' ? 0 : Number(steps);
    const userStatsAfterAddingActivity = addPoints(
      userInfo?.level,
      userInfo?.currentExperience,
      points,
    );
    setActivitiesPoints(
      activity,
      points,
      userId,
      duration,
      title,
      stepsDate,
      dateActivity,
    )
      .then(() => {
        updateUserDocumentField('/Users', userId, userStatsAfterAddingActivity)
          .then(() => {
            setUserInfo((prev) => {
              return { ...prev, ...userStatsAfterAddingActivity };
            });
            toast.success(
              'You have successfully created this activity',
              toastOptions,
            );
            addActivityToGoal(
              activity.toLowerCase(),
              duration,
              Points(),
              Number(steps),
            );
            setActivity('');
          })
          .catch(console.log);
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  };

  const Cancel = () => {
    setActivity('');
  };

  const addActivityToGoal = (activity, duration, points, steps) => {
    const goals = ACTIVITY_TO_GOAL[activity];
    Object.keys(goals).map((goal) =>
      checkIfGoalExists(sessionStorage.uid, goal)?.then((goalRef) => {
        let result;
        switch (goal) {
          case 'steps':
            result = steps;
            break;
          case 'points':
            result = points;
            break;
          case 'duration':
            result = Number(duration);
            break;
        }
        updateGoal(goalRef, goals[goal], result);
      }),
    );
  };

  const HistoryActivities = () => {
    setHistoryActivities(!historyActivities);
    getActivitiesByTime(userId)
      .then((resp) => {
        setDataHistory(resp);
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  };

  const HistoryClear = () => {
    setHistoryActivities(!historyActivities);
  };

  return (
    <>
      {loading ? (
        <div className='Spinner'>
          <ClipLoader
            color={'rgb(0, 45, 41)'}
            loading={loading}
            size={100}
            speedMultiplier='0.7'
          />
        </div>
      ) : (
        <div className='add-activities-container'>
          <h2>Activities Management</h2>
          <div className='add-activities'>
            <div className='scroll-div'>
              {activitiesData.map(({ uid, url, activity }) => {
                return (
                  <div
                    className='menu-item'
                    onClick={(e) => handleClick(e.target, activity)}
                    key={uid}>
                    <div className='head-text'>
                      <div className='head-image'>
                        <img key={uid} src={url} alt='' />
                      </div>
                      <div className='text-on-image'>
                        <div>{activity}</div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className='pick'>Select an Activity from the list</div>
          <div className='new-activity'>
            <div className='new-activity-box'>
              {historyActivities ? (
                <div>
                  <EditHistoryActivity
                    dataHistory={dataHistory}
                    setDataHistory={setDataHistory}
                  />
                </div>
              ) : activity ? (
                <>
                  <div className='new-activity-title'>{activity}</div>
                  <div>
                    <InputActivities
                      inputDate={{
                        duration,
                        setDuration,
                        title,
                        setTitle,
                        steps,
                        setSteps,
                        dateActivity,
                        setDateActivity,
                        activity,
                      }}
                      errors={{
                        titleError,
                        setTitleError,
                        durationError,
                        setDurationError,
                        stepsError,
                        setStepsError,
                      }}
                    />
                    <div className='new-activity-message'>
                      This activity is equal to {Points()} points
                    </div>
                    <button
                      disabled={
                        !(title && titleError === '') ||
                        !(duration && durationError === '') ||
                        !(CHECK_ACTIVITY || (steps && stepsError === ''))
                      }
                      onClick={() => {
                        Submit();
                      }}>
                      Submit
                    </button>
                    <button onClick={Cancel}>Cancel</button>
                  </div>
                </>
              ) : (
                <></>
              )}
            </div>
            <hr className='line1'></hr>
            <div className='history-activities'>
              <div className='pick'>Logged Activities for last 8 hour</div>
              {!historyActivities ? (
                <button onClick={HistoryActivities}>View</button>
              ) : (
                <div>
                  <button onClick={HistoryClear}>Close</button>
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </>
  );
};
