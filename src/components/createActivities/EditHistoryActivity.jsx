import './EditHistoryActivity.css';
import React, { useEffect, useState } from 'react';
import activityPoints from '../../utils/activityPointsCalculation';
import {
  getUserData,
  updateUserDocumentField,
} from '../../services/users.service';
import { toast } from 'react-toastify';
import {
  CHECK_ACTIVITY,
  DURATION_MAX_TIME,
  LIMIT_SEVEN_DAYS,
  MAX_STEPS,
  TITLE_MAX_LENGTH,
  TITLE_MIN_LENGTH,
  toastOptions,
} from '../../common/constants';
import {
  deleteActivityDocument,
  getActivitiesByTime,
  updateActivityDocumentFields,
} from '../../services/activities.service';
import { addPoints } from '../../utils/addExperience';
import moment from 'moment';
import { DatePicker } from 'antd';

export const EditHistoryActivity = (prop) => {
  const [currentTitle, setCurrentTitle] = useState('');
  const [currentDuration, setCurrentDuration] = useState('');
  const [currentDocId, setCurrentDocId] = useState('');
  const [currentActivity, setCurrentActivity] = useState('');
  const [currentSteps, setCurrentSteps] = useState('');
  const [currentDateActivity, setCurrentDateActivity] = useState('');
  const [currentPoints, setCurrentPoint] = useState(0);
  const [edit, setEdit] = useState(false);
  const [userInfo, setUserInfo] = useState({});
  const [titleError, setTitleError] = useState('');
  const [durationError, setDurationError] = useState('');
  const [stepsError, setStepsError] = useState('');

  const userId = sessionStorage.getItem('uid');

  const dateFormatList = ['DD/MM/YYYY', 'DD/MM/YY'];

  const DisableDates = (current) => {
    return (
      current &&
      (current < moment().endOf('day') - LIMIT_SEVEN_DAYS ||
        current > moment().endOf('day'))
    );
  };

  const handleInputDateActivity = (value) => {
    setCurrentDateActivity(value._d);
  };

  useEffect(() => {
    getUserData('/Users', userId)
      .then((resp) => {
        setUserInfo(resp.data());
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  }, []);

  const editActivity = (event, param) => {
    setCurrentTitle(param.title);
    setCurrentDuration(param.duration);
    setCurrentDocId(param.docId);
    setCurrentActivity(param.activity);
    setCurrentSteps(param.steps);
    setCurrentDateActivity(param.dateActivity);
    setCurrentPoint(param.points);
    setEdit(!edit);
  };

  const deleteActivity = (event, docId, activityPoints) => {
    const newUserData = addPoints(
      userInfo?.level,
      userInfo?.currentExperience,
      -activityPoints,
    );

    deleteActivityDocument(userId, docId)
      .then(() => {
        updateUserDocumentField('/Users', userId, newUserData).then(() => {
          setUserInfo((prev) => {
            return { ...prev, ...newUserData };
          });
          toast.success(
            'You have successfully delete this Activity',
            toastOptions,
          );
        });
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });

    getActivitiesByTime(userId)
      .then((resp) => {
        prop.setDataHistory(resp);
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  };

  const editClear = () => {
    setEdit(!edit);
    setTitleError('');
    setDurationError('');
    setStepsError('');
  };

  const Points = () =>
    activityPoints(currentDuration, currentActivity, userInfo.weight);

  const handleSave = () => {
    const points = activityPoints(
      currentDuration,
      currentActivity,
      userInfo.weight,
    );
    const pointsDelta = points - currentPoints;
    const newUserData = addPoints(
      userInfo?.level,
      userInfo?.currentExperience,
      pointsDelta,
    );

    const updateDate = {
      duration: currentDuration,
      title: currentTitle,
      steps: currentSteps,
      dateActivity: currentDateActivity,
      points,
    };

    updateActivityDocumentFields(
      `/Users/${userId}/activities`,
      currentDocId,
      updateDate,
    )
      .then(() => {
        updateUserDocumentField('/Users', userId, newUserData).then(() => {
          setUserInfo((prev) => {
            return { ...prev, ...newUserData };
          });
          toast.success(
            'You have successfully update this activity',
            toastOptions,
          );
          setEdit(!edit);

          getActivitiesByTime(userId)
            .then((resp) => {
              prop.setDataHistory(resp);
            })
            .catch((err) => {
              toast.error(err.message, toastOptions);
            });
        });
      })
      .catch((err) => {
        toast.error(err.message, toastOptions);
      });
  };

  const clearTitleError = () => {
    setTitleError('');
  };

  const validateTitle = () => {
    if (
      currentTitle.length < TITLE_MIN_LENGTH ||
      currentTitle.length > TITLE_MAX_LENGTH
    ) {
      setTitleError(
        `Title must be in range ${TITLE_MIN_LENGTH} - ${TITLE_MAX_LENGTH} symbols`,
      );
      return;
    }
  };

  const handleInputTitle = (event) => {
    setCurrentTitle(event.target.value);
  };

  const clearDurationError = () => {
    setDurationError('');
  };

  const validateDuration = () => {
    if (
      currentDuration < 0 ||
      currentDuration > DURATION_MAX_TIME ||
      currentDuration === ''
    ) {
      setDurationError(
        `Input valid duration max ${
          DURATION_MAX_TIME / 60
        } (${DURATION_MAX_TIME} min)`,
      );
      return;
    }
  };

  const handleInputDuration = (event) => {
    if (isNaN(event.target.value)) {
      setCurrentDuration(event.target.value.slice(0, -1));
      setDurationError('Input valid duration');
      return;
    } else {
      setDurationError('');
      setCurrentDuration(Number(event.target.value));
    }
  };

  const clearStepsError = () => {
    setStepsError('');
    if (isNaN(currentSteps)) {
      setCurrentSteps('');
    }
  };

  const validateSteps = () => {
    if (currentSteps < 0 || currentSteps > MAX_STEPS || currentSteps === '') {
      setStepsError(`Input valid number of steps  (max ${MAX_STEPS})`);
      return;
    }
  };

  const handleInputSteps = (event) => {
    if (isNaN(event.target.value)) {
      setCurrentSteps(event.target.value.slice(0, -1));
      setStepsError('Input valid number of steps');
      return;
    } else {
      setStepsError('');
      setCurrentSteps(Number(event.target.value));
    }
  };

  return (
    <div className='history-container'>
      {!edit ? (
        prop.dataHistory.map((activity) => {
          return (
            <div className='history-box' key={activity.docId}>
              <hr className='line2'></hr>
              <div className='activity-date-activity'>
                <div className='activity'>{activity.activity}</div>
                <div className='date-activity'>
                  {activity.dateActivity.toDate().toLocaleDateString('en-GB')}
                </div>
              </div>
              <div className='title'>{activity.title}</div>
              <div className='stats'>{activity.duration} min</div>
              {activity.steps ? (
                <div className='stats'>{activity.steps} steps</div>
              ) : (
                <></>
              )}
              <div className='stats'>{activity.points} activities points</div>
              <button onClick={(event) => editActivity(event, activity)}>
                Edit
              </button>
              <button
                onClick={(event) =>
                  deleteActivity(event, activity.docId, activity.points)
                }>
                Delete
              </button>
            </div>
          );
        })
      ) : (
        <div className='edit-box'>
          <div className='input-activity'>
            <div className='activity-name'>{currentActivity}</div>
            <label>Title</label>
            <input
              className='input-activity-title'
              type='text'
              value={currentTitle}
              onChange={handleInputTitle}
              onBlur={validateTitle}
              onFocus={clearTitleError}></input>
            <div className='input-distance'>
              <div className='error'>{titleError}</div>
            </div>
            <div className='duration-steps'>
              <div>
                <label>Duration</label>
                <div className='duration-min'>
                  <input
                    className='input-activity-duration'
                    type='text'
                    value={currentDuration}
                    onChange={handleInputDuration}
                    onBlur={validateDuration}
                    onFocus={clearDurationError}></input>
                  <div className='min'>min</div>
                </div>
                <div className='input-distance'>
                  <div className='error'>{durationError}</div>
                </div>
              </div>
              <div>
                {CHECK_ACTIVITY.includes(currentActivity) ? (
                  <>
                    <label>Steps</label>
                    <div className='steps'>
                      <input
                        className='input-activity-steps'
                        type='text'
                        value={currentSteps}
                        onChange={handleInputSteps}
                        onBlur={validateSteps}
                        onFocus={clearStepsError}></input>
                    </div>
                    <div className='input-distance'>
                      <div className='error'>{stepsError}</div>
                    </div>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
            <label>Date on Activity</label>
            <div className='edit-date-picker'>
              <DatePicker
                defaultValue={
                  isNaN(moment(currentDateActivity))
                    ? moment(currentDateActivity.toDate())
                    : moment(currentDateActivity)
                }
                format={dateFormatList}
                disabledDate={(current) => DisableDates(current)}
                onChange={handleInputDateActivity}
              />
            </div>
            <div className='input-distance'></div>
            <div className='new-activity-message'>
              This activity is equal to {Points()} points
            </div>
            <div className='buttons'>
              <button
                onClick={handleSave}
                disabled={
                  !(currentTitle && titleError === '') ||
                  !(currentDuration && durationError === '') ||
                  !(CHECK_ACTIVITY && currentSteps && stepsError === '')
                }>
                Save
              </button>
              <button onClick={editClear}>Cancel</button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
