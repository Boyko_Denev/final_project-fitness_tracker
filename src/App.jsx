import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { useState } from 'react';
import Signup from './views/SignIn/Signup';
import Login from './views/SignIn/Login';
import Logout from './views/SignIn/Logout';
import AppState from './providers/app-state';
import Authenticated from './hoc/Authenticated';
import Profile from './views/Profile/ProfilePage';
import Home from './views/Home/Home';
import { Sidebar } from './components/sidebar/Sidebar';
import { Dashboard } from './views/Dashboard/Dashboard.jsx';
import { Goals } from './views/Goals/Goals.jsx';
import { AddActivities } from './views/AddActivities/AddActivities';
import { Friends } from './views/Friends/Friends';
import { Support } from './views/Support/Support';
import { ProfileCompleted } from './views/ProfileCompleted/ProfileCompleted';
import Users from './views/Users/Users';
import { Error404 } from './views/PageNotFound/PageNotFound';
import { ToastContainer } from 'react-toastify';
import AboutUs from './views/AboutUs/AboutUs';

const App = () => {
  const [appState, setState] = useState({
    user: null,
    userData: null,
  });

  return (
    <Router>
      <ToastContainer />
      <div className='background'>
        <AppState.Provider value={{ appState, setState }}>
          <Sidebar>
            <Routes>
              <Route exact path='/' element={<Home />} />
              <Route
                path='profile/:uid'
                element={
                  <Authenticated>
                    <Profile />
                  </Authenticated>
                }
              />
              <Route
                path='/users'
                element={
                  <Authenticated>
                    <Users />
                  </Authenticated>
                }
              />
              <Route path='/profile-completed' element={<ProfileCompleted />} />
              <Route path='/signup' element={<Signup />} />
              <Route path='/login' element={<Login />} />
              <Route path='/logout' element={<Logout />} />
              <Route
                path='/board'
                element={
                  <Authenticated>
                    <Dashboard />
                  </Authenticated>
                }
              />
              <Route
                path='/set-goals'
                element={
                  <Authenticated>
                    <Goals />
                  </Authenticated>
                }
              />
              <Route
                path='/add-activities'
                element={
                  <Authenticated>
                    <AddActivities />
                  </Authenticated>
                }
              />
              <Route
                path='/friends'
                element={
                  <Authenticated>
                    <Friends />
                  </Authenticated>
                }
              />
              <Route path='/about-us' element={<AboutUs />} />
              <Route
                path='/support'
                element={
                  <Authenticated>
                    <Support />
                  </Authenticated>
                }
              />
              <Route path='*' element={<Error404 />} />
            </Routes>
          </Sidebar>
        </AppState.Provider>
      </div>
    </Router>
  );
};

export default App;
