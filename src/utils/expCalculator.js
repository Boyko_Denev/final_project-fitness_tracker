const nextLevel=(level)=>{
    return 100*Math.round(level*5/(1+Math.exp((-level/10)+1)))
}
export default nextLevel