import moment from "moment"
import { defaultDateFormat } from "../common/constants"
export const extractDate=(timeInS)=>moment(
    new Date(timeInS * 1000),).format(defaultDateFormat)