//time in minutes
//activity - base points per minute
// factoring weight of the user
import { activityPointsRanges, weightRange } from '../common/constants';

const activityPoints = (time, activity, weight) => {
  const deltaActivity =
    activityPointsRanges[activity][1] - activityPointsRanges[activity][0];
  const deltaWeight = weightRange[1] - weightRange[0];
  const slope = deltaActivity / deltaWeight;
  return Math.round(
    time *
    (slope * weight +
      activityPointsRanges[activity][0] -
      slope * weightRange[0])
  );
};

export default activityPoints;
