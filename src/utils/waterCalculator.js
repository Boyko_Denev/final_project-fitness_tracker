const waterLimitDaily = (weight) => {
  return (Math.pow((weight / 18), 0.7) + (weight / 80))
}

export default waterLimitDaily;