import  nextLevel from './expCalculator'

export const addPoints = (currLevel, currentExp, pointToBeAdded) => {
     
    if (currentExp + pointToBeAdded < 0) {
      const expNextLevel = nextLevel(currLevel-1);
      return addPoints(
        currLevel - 1,
        expNextLevel,
        currentExp + pointToBeAdded,
      );
    }
    const expNextLevel = nextLevel(currLevel);
    const expNeeded = expNextLevel - currentExp;
  
    if (pointToBeAdded >= expNeeded) {
  
      return addPoints(currLevel + 1, 0, pointToBeAdded - expNeeded);
    }
  
    return { level: currLevel, currentExperience: pointToBeAdded + currentExp };
  };