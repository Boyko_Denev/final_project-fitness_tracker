import { createContext } from 'react';


const AppState = createContext({
  appState: {
    user: null,
    userData: null,
    friends: null
  },
  setState: () => { },
});

export default AppState;