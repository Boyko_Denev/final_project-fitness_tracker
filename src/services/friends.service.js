import {
  doc,
  setDoc,
  collection,
  getDocs,
  query,
  onSnapshot,
  where,
  deleteDoc,
  updateDoc
} from 'firebase/firestore';
import { db } from '../config/firebase-config';
import { friendRequestStatus } from '../common/constants';

export const deleteFriend = async (senderUid, receiverUid) => {
  try {
    await deleteDoc(doc(db, `Users/${receiverUid}/friends`, senderUid));
    return true;
  } catch (e) {
    console.log(e);
    return false;
  }
};

export const getFriendsWithGivenStatus= async (uid,status)=>{

    const data = await getDocs(query(collection(db, `Users/${uid}/friends`), where('status', '==', status)))
    const result = [];
    data.forEach((doc) => {
        result.push(doc.data());
        
    });
    return result
  
}
export const AddFriend = async (senderUid, receiverUid,status,isSender=false) =>{
  await setDoc(doc(db, `/Users/${receiverUid}/friends/${senderUid}`), {
    sender:senderUid,
    status:status,
    isOrigin:isSender
  })}

export const updateFriendFields = async (senderUid, receiverUid,data)=>{
  return await updateDoc(doc(db, `/Users/${receiverUid}/friends/${senderUid}`), data)
}
export const unsubscribe = (uid)=>{
    onSnapshot(collection(db, `/Users/${uid}/friends`),snapshot=>{
        console.log(snapshot.docs)
    })
}

export const getAllFriends = async (uid)=>{
const docs = await getDocs(query(collection(db, `Users/${uid}/friends`)))
const result = [];
docs.forEach((doc) => {
    result.push(doc.data());
    
});
return result
}