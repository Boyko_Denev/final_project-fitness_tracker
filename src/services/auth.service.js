import { signInWithPopup, GoogleAuthProvider, deleteUser, sendPasswordResetEmail } from 'firebase/auth'
import { authProvider1 } from '../config/firebase-config.js'
import { auth } from '../config/firebase-config';
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
  updateProfile
} from 'firebase/auth';
import { async } from '@firebase/util';

export const signupUser = (email, password) => {
  return createUserWithEmailAndPassword(auth, email, password)
};

export const loginUser = (email, password) => {
  return signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      const user = userCredential.user;

      return user;
    })
};

export const logoutUser = () => {
  return signOut(auth);
};

export const updateUserDisplayName = (username) => {
  return updateProfile(auth.currentUser, {
    displayName: username
  })
}

export const loginGooglePopUp = () => {
  return signInWithPopup(auth, authProvider1)
    .then((result) => {
      // This gives you a Google Access Token. You can use it to access the Google API.
      const credential = GoogleAuthProvider.credentialFromResult(result);
      const token = credential.accessToken;
      // The signed-in user info.
      const user = result.user;
      return user
      // ...
    }).catch((error) => {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      // The email of the user's account used.
      const email = error.customData.email;
      // The AuthCredential type that was used.
      const credential = GoogleAuthProvider.credentialFromError(error);
      // ...
      return(errorMessage)
    });
}

export const forgottenPassword = (email) => {
  return sendPasswordResetEmail(auth, email)
}
