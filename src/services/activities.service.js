import {
  addDoc,
  collection,
  deleteDoc,
  doc,
  getDocs,
  limit,
  orderBy,
  query,
  serverTimestamp,
  updateDoc,
  where,
} from "firebase/firestore";
import { EIGHT_HOURS, RECENT_ACTIVITIES_NUMBER } from "../common/constants";
import { db } from "../config/firebase-config";

// Get all Activities
export const getAllActivities = () => {

  const data = query(collection(db, `Activities`))

  return (
    getDocs(data)
      .then((querySnapshot) => {
        const data = []
        querySnapshot.forEach(doc => {
          data.push(doc.data())
        })

        return data
      })
  )
}

// Set Activity points
export const setActivitiesPoints = (activity, points, uid, duration, title, steps, dateActivity) =>
  addDoc(collection(db, `/Users/${uid}/activities`), {
    activity,
    points,
    duration,
    title,
    steps,
    date: serverTimestamp(),
    dateActivity,
  })

// Get user's Activities for last 8h
export const getActivitiesByTime = (uid) => {
  const moment = require('moment');

  const time = moment() - (EIGHT_HOURS)

  const data = query(collection(db, `Users/${uid}/activities`), where('date', '>', new Date(time)))

  return (
    getDocs(data)
      .then(querySnapshot => {
        const activities = []
        querySnapshot.forEach(doc => {
          const docId = doc.id
          activities.push({ ...doc.data(), docId })
        })

        return activities
      })
  )
}

// Get Recent Activities
export const getRecentActivities = (uid) => {
  const data = query(collection(db, `Users/${uid}/activities`), orderBy('dateActivity', 'desc'), limit(RECENT_ACTIVITIES_NUMBER))

  return (
    getDocs(data)
      .then(querySnapshot => {
        const activities = []
        querySnapshot.forEach(doc => {
          const docId = doc.id
          activities.push({ ...doc.data(), docId })
        })

        return activities
      })
  )
}

// Get user's activities
export const getUserActivities = (uid) => {
  const q = query(collection(db, `Users/${uid}/activities`))
  return (
    getDocs(q)
      .then(querySnapshot => {
        const activities = []
        querySnapshot.forEach(doc => {
          activities.push(doc.data())
        })

        return activities
      }))
}
// Update Activity
export const updateActivityDocumentFields = (col, docId, userData) => {
  return updateDoc(doc(db, col, docId), userData)
}

// Delete Activity
export const deleteActivityDocument = (uid, docId) => {
  return deleteDoc(doc(db, `Users/${uid}/activities/${docId}`))
}
