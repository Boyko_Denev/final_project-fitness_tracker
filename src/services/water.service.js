import {
  addDoc,
  collection,
  getDocs,
  query,
  serverTimestamp,
  where
} from "firebase/firestore";
import { WATER_INTAKE_PERIOD } from "../common/constants";
import { db } from "../config/firebase-config";
// import { toast } from 'react-toastify';
// import { toastOptions } from '../../common/constants';



// - 1 - Add water volume in new doc in FB water collection
export const AddWater = (volume, uid) =>
  addDoc(collection(db, `/Users/${uid}/water`), {
    volume: volume,
    date: serverTimestamp(),
  })

// - 2 - Get all docs from water collection for present day

export const GetWaterPresentDay = (uid) => {
  const moment = require('moment');

  let startDay = moment()
  startDay.startOf('day')

  const data = query(collection(db, `Users/${uid}/water`), where('date', '>', startDay.toDate()))

  return (
    getDocs(data)
      .then(querySnapshot => {
        let water = 0
        querySnapshot.forEach((doc) => {
          water = water + doc.data().volume
        })
        return water
      })
  )
}

// - 3 - Get Water intake per day for last n days

export const getWaterIntakePerDay = (uid, day) => {
  const moment = require('moment');

  // for (let i = WATER_INTAKE_PERIOD - 1; i >= 0; --i) {
  let startDay = moment().startOf('day').subtract(day, 'days').toDate();
  let endDay = moment().endOf('day').subtract(day, 'days').toDate();

  let data = query(collection(db, `Users/${uid}/water`), where('date', '>', startDay), where('date', '<', endDay))
  return (

    getDocs(data)
      .then(querySnapshot => {
        let water = 0
        let waterData = {}

        querySnapshot.forEach((doc) => {
          water = water + doc.data().volume
        })

        waterData = {
          date: startDay.toLocaleDateString('en-GB').slice(0, 5),
          Water: water
        }

        // console.log(waterData);
        return waterData;
      })
  )
}