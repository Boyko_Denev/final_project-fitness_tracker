import {
  collection, deleteDoc, doc, getDoc,
  getDocs,
  query,
  serverTimestamp, setDoc, updateDoc,increment
} from "firebase/firestore";
import { db } from "../config/firebase-config";
import moment from 'moment';
import { toast } from 'react-toastify';
import {toastOptions} from '../common/constants.js';

export const addGoalToDb = (name,uid, start, end, target,type) =>
  setDoc(doc(db, `/Users/${uid}/goals`,type), {
    start,
    end,
    target,
    name,
    isActive:false,
    createDate: serverTimestamp(),
    // dont like next approach
    totalSteps: 0,
    totalPoints: 0,
    totalDuration: 0
  })


export const getAllGoalsFromDb = () =>{
  const data = query(collection(db, '/Goals'))
  return (
    getDocs(data)
      .then((querySnapshot) => {
        const goals = []
        querySnapshot.forEach(doc => {
          goals.push({...doc.data(),goalName:doc.id})
        })

        return goals
      })
  )

}

export const updateGoal = (document,type,value)=>{
  document && updateDoc(document,{
    [type]:increment(value)
  }).then(()=>getGoalByRef(document).then(goalObj=>{
    const goal=goalObj.data()
    goal.target < goal[type] && toast.success(
      `You have completed your goal:${goal.name}`,
      toastOptions,
    );
  })).catch(err=>err)
}

export const checkIfGoalExists = (uid,type) =>{
  return getDoc(doc(db,`/Users/${uid}/goals`,type)).then(goal =>  {
    return goal.data() === undefined ? null : goal.ref
  }
  )
}
export const getUserGoals = (uid)=>{
  return getDocs(query(collection(db, `Users/${uid}/goals`))).then(goals=>{
    const result = []
    goals.forEach(el=>{
      result.push({...el.data(),type:el.id})})
    return result
  })
}
export const getGoalByRef = (ref) =>{
  return getDoc(ref)
}
export const deleteGoalById =  (db, col, userId) => {
   return deleteDoc(doc(db, col, userId)).catch(err=>err);
};

export const initGoal = (uid,type) => {
  return setDoc(doc(db, `/Users/${uid}/goals`,type), {
    start:moment().toDate(),
    end:moment().toDate(),
    target:0,
    isActive:false,
    createDate: serverTimestamp(),
    totalSteps: 0,
    totalPoints: 0,
    totalDuration: 0
  }).catch(err=>err)
}
export const switchGoal = (uid,type) =>{
  checkIfGoalExists(uid,type)?.then(result=>{
    if(!result){
      initGoal(uid,type).catch(err=>err)
    }else{
      deleteGoalById(db,`/Users/${uid}/goals`,type).catch(err=>err)
    }
  })
}