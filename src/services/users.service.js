import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from 'firebase/auth';
import { auth, db } from '../config/firebase-config';
import {
  addDoc,
  collection,
  deleteDoc,
  deleteField,
  doc,
  getDoc,
  getDocs,
  query,
  serverTimestamp,
  setDoc,
  updateDoc,
  where,
} from 'firebase/firestore';
import { userRole, gender } from '../common/constants';

// Get user credential on login
export const logInGetUser = (email, password) => {
  return signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      const user = userCredential.user;

      return user;
    })
  // .catch((error) => {
  //   return error;
  // });
};

// Sign Up new user

export const createUser = ({ email, password }) => {
  return createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      const user = userCredential.user;

      return user;
    })
    .catch((error) => {
      return error.message;
    });
};

// ************** ADD & UPDATE data ***********

export const addDocument = async (col, data) =>
  await addDoc(collection(db, col), data);

// create or update document if exists; if { merge: true } is passed data will be merged
export const updateUserById = async (col, uid, dataDoc) =>
  await setDoc(doc(db, col, uid), dataDoc, { merge: true });

// add user with random generated id and returns  reference to newly created user document
export const addUser = async (col, userData) => {
  const userRef = await addDoc(collection(db, col), userData);
  return userRef;
};

export const updateUserDocumentFields = (col, docId, userData) => {
  return updateDoc(doc(db, col, docId), userData)
}

export const updateUserDocumentField = async (col, docId, data) => {
  const userRef = doc(db, col, docId);
  await updateDoc(userRef, data);
};

// Delete user by id
export const deleteUserById = async (col, userId) => {
  await deleteDoc(doc(db, col, userId));
};
// Delete specific field from document
export const deleteUserDocumentField = async (col, userId, arr) => {
  const getUserRef = doc(db, col, userId);
  await updateDoc(
    getUserRef,
    arr.reduce((acc, cur) => {
      acc = { ...acc, cur: deleteField() };
      return acc;
    }, {}),
  );
};

// Get user's weight
export const getUserWeight = (userId) => {
  const data = doc(db, `/Users`, userId)

  return (
    getDoc(data)
      .then(querySnapshot => {
        return querySnapshot.data().weight
      })
    // .catch((err) => {
    //   return err
    // })
  )
}

// return document reference path approach :) don't like it
export const getDocumentByIdPath = (col, uid) => {
  return doc(db, `${col}/${uid}`);
};

// Returns all usernames to check

export const getUserByUsername = async (username) => {
  const q = query(collection(db, '/Users'), where('userName', '==', username));
  const querySnapshot = await getDocs(q);
  return querySnapshot || null;
};

export const getUserById = async (userId) => {
  const docRef = doc(db, '/Users', userId);
  const querySnapshot = await getDoc(docRef);
  return querySnapshot || null;
};

// Create User

export const createUserUsername = (userName, phoneNumber, uid, email,provider) => {
  return (
    setDoc(doc(db, `/Users`, uid), {
      uid,
      userName,
      email,
      phoneNumber,
      provider,
      isProfileCompleted: false,
      achievements: [],
      age: 0,
      avatarUrl: '',
      currentExperience: 0,
      friendList: [],
      gender: gender.NOT_ASSIGNED,
      groups: [],
      height: 0,
      level: 1,
      quests: [],
      water: 0,
      weight: 0,
      info: '',
      activities: '',
      firstName: '',
      lastName: '',
      createData: serverTimestamp(),
      role: userRole.BASIC,
      isBlocked: false,
    }));
};

// Returns document content
export const getUserData = (col, userId) => {
  const docRef = doc(db, col, userId);
  return getDoc(docRef);
};
// export const getUserData = async (col, userId) => {
//   const docRef = doc(db, col, userId);
//   const docSnap = await getDoc(docRef);
//   return docSnap || null;
// };
export const getUsersData = async (userIds) => {
  const docSnap = await getDocs(query(collection(db, '/Users'), where('uid', 'in', userIds)));
  const array = [];
  docSnap.forEach((doc) => {
    array.push(doc.data());
  });
  return array;
};
export const getAllUsers = async () => {
  const q = query(collection(db, '/Users'));
  const docSnap = await getDocs(q);
  const array = [];
  docSnap.forEach((doc) => {
    array.push(doc.data());
  });
  return array;
};
