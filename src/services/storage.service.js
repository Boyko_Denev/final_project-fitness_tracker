import { getDownloadURL, ref, uploadBytes } from "firebase/storage"
import { storage } from "../config/firebase-config"
export const upload = async (file, fileName, path)=>{
const fileRef = ref(storage, `${path}/${fileName}`)
await uploadBytes(fileRef, file)
const photoUrl = await getDownloadURL(fileRef)
return photoUrl
}