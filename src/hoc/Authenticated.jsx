import { useContext } from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import AppState from '../providers/app-state';

const Authenticated = ({ children }) => {
  const { appState } = useContext(AppState);
  const user = sessionStorage.getItem('userName');
  const location = useLocation();

  if (!user) {
    return <Navigate to='/' state={{ from: location.pathname }} />;
  }

  return children;
};

export default Authenticated;
